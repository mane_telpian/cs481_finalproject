import 'package:finalproject/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'sideMenu.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dogCategory.dart';
import 'catCategory.dart';
import 'reptilesCategory.dart';
import 'smallanimalsCategory.dart';

class displayInfoForAnimal extends StatefulWidget {
  displayInfoForAnimal({this.name, this.imageType, this.imageIndex, this.age, this.Health,
    this.email, this.address, this.phoneNumber, this.shelterName, this.breed, this.description, this.FavoritedVariable, this.imageName});
  final String name;
  final String imageIndex;
  final String imageType;
  final String age;
  final String Health;
  final String email;
  final String address;
  final String phoneNumber;
  final String shelterName;
  final String breed;
  final String description;
  bool FavoritedVariable;
  final String imageName;

  @override
  _displayInfoForAnimalState createState() => _displayInfoForAnimalState();
}

class _displayInfoForAnimalState extends State<displayInfoForAnimal> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void determineCategory(String categoryName) {
    print(categoryName);

    if(categoryName == 'dog') {
      Navigator.of(context).popUntil((route) => route.isFirst);
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => DogCategory()));
    }

    if(categoryName == 'cat') {
      Navigator.of(context).popUntil((route) => route.isFirst);
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => catCategory()));
    }

    if(categoryName == 'reptile') {
      Navigator.of(context).popUntil((route) => route.isFirst);
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => reptileCategory()));
    }

    if(categoryName == 'smallanimal') {
      Navigator.of(context).popUntil((route) => route.isFirst);
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => smallanimalCategory()));
    }
  }

  createAlertDialog(BuildContext context) {
    TextEditingController controller = TextEditingController();
    return showDialog(context: context, builder: (context) {
      return AlertDialog(
        title: Text('Emailing ' + widget.shelterName,
            style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Colors.blueGrey[900])),
        ),
        content: TextField(
          controller: controller,
        ),
        actions: <Widget>[
          MaterialButton(
            elevation: 5.0,
            child: Text('Send the Email!',
                style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Colors.blueGrey[900])),
            ),
            onPressed: () {
              setState(() {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  backgroundColor: Colors.green[800],
                  content: Text('Email sent to ' + widget.shelterName,
                      style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Colors.white)),
                  ),
                  duration: Duration(seconds: 10),
                ));
                Navigator.of(context).pop();
              });
            },
          )
        ],
      );
    },);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.name),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.white,
            onPressed: () => determineCategory(widget.imageName),
          ),
        ],
        backgroundColor: Colors.green[800],
      ),
      body: ListView(
        children: <Widget>[
          Container(
            height: 200,
            width: 200,
            child: Container(
              height: 200,
              width: 200,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/' + widget.imageName + widget.imageIndex + widget.imageType),
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),

          Container(
            height: 50,
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(widget.age + ' - ' + widget.breed, style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.blueGrey[900]))),
              ],
            ),
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 0.5,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.deepPurple[600],
                      Colors.deepPurple[400],
                      Colors.deepPurple[200],
                      Colors.deepPurple[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          Row(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Text('Breed', style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.blueGrey[900]))),
              )
            ],
          ),

          Row(
            children: [
              Container(
                  height: 50,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(Icons.library_books),
                      Container(width: 5),
                      Text(widget.breed, style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Colors.blueGrey[900]))),
                    ],
                  )
              )
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 0.5,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.deepPurple[600],
                      Colors.deepPurple[400],
                      Colors.deepPurple[200],
                      Colors.deepPurple[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          Row(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Text('Health', style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.blueGrey[900]))),
              )
            ],
          ),

          Row(
            children: [
              Container(
                  height: 50,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(Icons.healing),
                      Container(width: 5),
                      Text(widget.Health, style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Colors.blueGrey[900]))),
                    ],
                  )
              )
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 0.5,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.deepPurple[600],
                      Colors.deepPurple[400],
                      Colors.deepPurple[200],
                      Colors.deepPurple[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          Row(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Text('Description', style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.blueGrey[900]))),
              )
            ],
          ),

          Row(
            children: [
              Container(
                  height: 50,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(Icons.textsms),
                      Container(width: 5),
                      Text(widget.description, style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Colors.blueGrey[900]))),
                    ],
                  )
              )
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 2,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.brown[100],
                      Colors.brown[100],
                      Colors.brown[100],
                      Colors.brown[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          Center(
            child: Container(
              height: 75,
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(widget.shelterName, style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.blueGrey[900]))),
                ],
              ),
            ),
          ),

          Column(
            children: [
              Row(
                children: [
                  Container(
                      height: 50,
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(Icons.pin_drop),
                          Container(width: 5),
                          Text(widget.address, style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Colors.blueGrey[900]))),
                        ],
                      )
                  )
                ],
              ),

              Row(
                children: [
                  Container(
                      height: 50,
                      padding: EdgeInsets.all(10),
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            createAlertDialog(context);
                          });
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(Icons.email),
                            Container(width: 5),
                            Text(widget.email, style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.w500, color: Colors.blueGrey[900]))),
                          ],
                        ),
                      )
                  )
                ],
              ),

              Row(
                children: [
                  Container(
                      height: 50,
                      padding: EdgeInsets.all(10),
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            _scaffoldKey.currentState.showSnackBar(SnackBar(
                              backgroundColor: Colors.green[800],
                              content: Text('Dialing ' + widget.phoneNumber + ' . . . ',
                                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 25, fontWeight: FontWeight.w500, color: Colors.white)),
                              ),
                              duration: Duration(seconds: 10),
                            ));
                          });
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(Icons.phone),
                            Container(width: 5),
                            Text(widget.phoneNumber, style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Colors.blueGrey[900]))),
                          ],
                        ),
                      )
                  )
                ],
              ),
            ],
          ),


        ],
      ),
    );
  }
}