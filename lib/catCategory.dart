import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'main.dart';
import 'sideMenu.dart';
import 'catFilters.dart';
import 'extraVariables.dart';
import 'displayInfoOnAnAnimal.dart';
import 'package:google_fonts/google_fonts.dart';

///cat Category Page
///Gridlist of Cards for cats
///-----------------------------------------------------------------------------
class catCategory extends StatefulWidget {
  @override
  _catCategoryState createState() => _catCategoryState();
}

class _catCategoryState extends State<catCategory> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  ///Returns to Explore Page
  ///---------------------------------------------------------------------------
  void openHomePage() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => HomePage()));
  }

  ///Opens Filter Page
  ///---------------------------------------------------------------------------
  void opencatFilters() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => catFilters()));
  }

  ///Displays Info on the cat Card Selected
  ///---------------------------------------------------------------------------
  void openDisplayClass(String name, String index, String type, String age,
      String breed, String health, String shelterName, String address,
      String email, String phoneNumber, String description, bool favorited, String imageName) {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => displayInfoForAnimal(
      name: name,
      imageIndex: index,
      imageType: type,
      age: age,
      breed: breed,
      Health: health,
      shelterName: shelterName,
      address: address,
      email: email,
      phoneNumber: phoneNumber,
      description: description,
      FavoritedVariable: favorited,
      imageName: imageName,
    )));
  }

  ///Creates a Card
  ///---------------------------------------------------------------------------
  Card createAcatCard(String categoryType, String catName, String imageIndex,
      String imageType, String age, String breed, String health,
      String shelterName, String address, String email, String phoneNumber, String description,
      bool favorited) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: GestureDetector(
        onTap: () => openDisplayClass(catName, imageIndex, imageType, age,
            breed, health, shelterName, address, email, phoneNumber, description, favorited, categoryType),
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/' + categoryType + '$imageIndex' + imageType),
                fit: BoxFit.fill,
              ),
              borderRadius: BorderRadius.all(Radius.circular(20))
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Explore Cats',
            style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.white))),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.filter_list),
            color: Colors.white,
            tooltip: 'Open Filters for Cats!',
            onPressed: () => opencatFilters(),
          ),
          IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.white,
            tooltip: 'Return to Explore Page!',
            onPressed: () => openHomePage(),
          ),
        ],
        backgroundColor: Colors.green[800],
      ),
      body: showAllCATS? ListView(
        children: [
          createAcatCard('cat', 'Charlie', '0', '.jpg', 'Charlie', 'Persian',
                'Male, 8 yrs, Neutered', 'Monterey Shelter',
                '241 Los Gatos, CA 95521',
                'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Charlie is friendly and relaxed', Charlie),
          createAcatCard('cat', 'Bella', '1', '.jpeg', 'Bella', 'Persian',
              'Female, 4 yrs, Spayed', 'Santa Barbra Shelter',
              '900 Transient, CA, 94112',
              'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Bella is spunky and loves to play.', Bella),
          createAcatCard('cat', 'Jack', '2', '.jpg', 'Jack', 'Persian',
              'Male, 5 yrs, Neutered', 'San Diego Shelter',
              '811 Franklin Street, CA, 92541',
              'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Jack is laid back and easy to handle', Jack),
          createAcatCard('cat', 'Sammy', '4', '.jpg', 'Sammy', 'Siamese',
              'Male, 3 yrs, Neutered', 'Santa Barbra Shelter',
              '900 Transient, CA, 94112',
              'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Sammy is energetic & loves play', Sammy),
          createAcatCard('cat', 'Chloe', '5', '.jpg', 'Chloe', 'Siamese',
              'Female, 3 yrs, Spayed', 'Redding Shelter',
              '798 RedOak Street, CA, 90812',
              'ReddingShelter@FuRiends.co.net', '(530)-653-0096', 'Chloe takes time to open up', Chloe),
          createAcatCard('cat', 'Lola', '6', '.jpg', 'Lola', 'Siamese',
              'Female, 6 mths, Spayed', 'San Marcos Shelter',
              '333 S Twin Oaks Valley Rd, CA, 92096',
              'SanMarcosShelter@FuRiends.co.net', '(512)-252-2543', 'Lola loves to cuddle.', Lola),
          createAcatCard('cat', 'Leo', '7', '.jpg', 'Leo', 'Sphynx',
              'Male, 1 yrs, Neutered', 'San Francisco',
              '125 Old Pier, CA, 91123',
              'SanFranciscoShelter@FuRiends.co.net', '(415)-112-6531', 'Leo has a big personality.', Leo),
          createAcatCard('cat', 'Albert', '8', '.jpg', 'Albert', 'Sphynx',
              'Male, 3 yrs, Neutered', 'San Diego Shelter',
              '811 Franklin Street, CA, 92541',
              'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Albert enjoys the finer things in life.', albert),
          createAcatCard('cat', 'Louie', '9', '.jpg', 'Louie', 'Sphynx',
              'Male, 2 yrs, Neutered', 'Redding Shelter',
              '798 RedOak Street, CA, 90812',
              'ReddingShelter@FuRiends.co.net', '(530)-653-0096', 'Louie loves other animals', louie),
          createAcatCard('cat', 'Cleo', '10', '.jpg', 'Cleo', 'Sphynx',
              'Female, 4 yrs, Spayed', 'Santa Barbra Shelter',
              '900 Transient, CA, 94112',
              'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Cloe loves to play all day.', cleo),
          createAcatCard('cat', 'Missy', '11', '.jpg', 'Missy', 'Sphynx',
              'Female, 7 yrs, Spayed', 'San Francisco Shelter',
              '125 Old Pier, CA, 91123',
              'SanFranciscoShelter@FuRiends.co.net', '(415)-112-6531', 'Enjoys the company of others', missy),
          createAcatCard('cat', 'Ollie', '12', '.jpg', 'Ollie', 'Sphynx',
              'Male, 4 mths, Not Neutered', 'Monterey Shelter',
              '241 Los Gatos, CA 95521',
              'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Ollie is waiting for his forever home.', ollie),
          createAcatCard('cat', 'Kali', '13', '.jpg', 'Kali', 'Sphynx',
              'Female, 9 yrs, Spayed', 'Los Angeles Shelter',
              '699 HollyWood Blvd, CA, 97621',
              'LosAngelesShelter@FuRiends.co.net', '(213)-647-6751', 'Kali has a lot of love to give.', kali),
          createAcatCard('cat', 'Jax', '14', '.jpg', 'Jax', 'Sphynx',
              'Male, 5 months, Not Neutered', 'Los Angeles Shelter',
              '699 HollyWood Blvd, CA, 97621',
              'LosAngelesShelter@FuRiends.co.net', '(213)-647-6751', 'Jax is a little mischievous.', jax),
          createAcatCard('cat', 'Blue', '15', '.jpg', 'Blue', 'Sphynx',
              'Male, 6 yrs, Neutered', 'San Marcos Shelter',
              '333 S Twin Oaks Valley Rd, CA, 92096',
              'SanMarcosShelter@FuRiends.co.net', '(512)-252-2543', 'Blue enjoys cuddling & belly rubs', blue),
          createAcatCard('cat', 'Rosie', '16', '.jpg', 'Rosie', 'Egyptian Mau',
              'Female, 5 yrs, Spayed', 'Monterey Shelter',
              '241 Los Gatos, CA 95521',
              'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Rosie loves the outdoors', rosie),
          createAcatCard('cat', 'Gus', '17', '.jpg', 'Gus', 'Egyptian Mau',
              'Male, 7 yrs, Neutered', 'San Diego Shelter',
              '811 Franklin Street, CA, 92541',
              'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Gus loves to look out windows', gus),
          createAcatCard('cat', 'Ella', '18', '.jpg', 'Ella', 'Egyptian Mau',
              'Female, 8 yrs, Spayed', 'Los Angeles Shelter',
              '699 HollyWood Blvd, CA, 97621',
              'LosAngelesShelter@FuRiends.co.net', '(213)-647-6751', 'Ella is an old girl with lots of energy.', ella),
          createAcatCard('cat', 'Riley', '19', '.jpg', 'Riley', 'Birman',
              'Female, 9 mths, Spayed', 'San Marcos Shelter',
              '333 S Twin Oaks Valley Rd, CA, 92096',
              'SanMarcosShelter@FuRiends.co.net', '(512)-252-2543', 'Riley loves to relax', riley),
          createAcatCard('cat', 'Theo', '20', '.jpg', 'Theo', 'Birman',
              'Male, 8 yrs, Neutered', 'Redding Shelter',
              '798 RedOak Street, CA, 90812',
              'ReddingShelter@FuRiends.co.net', '(530)-653-0096', 'Theo loves company', theo),
          createAcatCard('cat', 'Tucker', '21', '.jpg', 'Tucker', 'Birman',
              'Male, 7 mths, Neutered', 'Santa Barbra Shelter',
              '900 Transient, CA, 94112',
              'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Tucker is small but mighty.', Tucker),
        ],
      ) : ListView(
        children: [
          Visibility(
            visible: persian || monterey,
            child: createAcatCard('cat', 'Charlie', '0', '.jpg', 'Charlie', 'Persian',
                'Male, 8 yrs, Neutered', 'Monterey Shelter',
                '241 Los Gatos, CA 95521',
                'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Charlie is friendly and relaxed', Charlie),
          ),
          Visibility(
            visible: persian || santabarbra,
            child: createAcatCard('cat', 'Bella', '1', '.jpeg', 'Bella', 'Persian',
                'Female, 4 yrs, Spayed', 'Santa Barbara Shelter',
                '900 Transient, CA, 94112',
                'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Bella is spunky and loves to play.', Bella),
          ),
          Visibility(
            visible: persian || sandiego,
            child: createAcatCard('cat', 'Jack', '2', '.jpg', 'Jack', 'Persian',
                'Male, 5 yrs, Neutered', 'San Diego Shelter',
                '811 Franklin Street, CA, 92541',
                'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Jack is laid back and easy to handle', Jack),
          ),
          Visibility(
            visible: siamese || santabarbra,
            child: createAcatCard('cat', 'Sammy', '4', '.jpg', 'Sammy', 'Siamese',
                'Male, 3 yrs, Neutered', 'Santa Barbra Shelter',
                '900 Transient, CA, 94112',
                'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Sammy is energetic & loves to play.', Sammy),
          ),
          Visibility(
            visible: siamese || redding,
            child: createAcatCard('cat', 'Chloe', '5', '.jpg', 'Chloe', 'Siamese',
                'Female, 3 yrs, Spayed', 'Redding Shelter',
                '798 RedOak Street, CA, 90812',
                'ReddingShelter@FuRiends.co.net', '(530)-653-0096', 'Chloe takes time to open up', Chloe),
          ),
          Visibility(
            visible: siamese || sanmarcos,
            child: createAcatCard('cat', 'Lola', '6', '.jpg', 'Lola', 'Siamese',
                'Female, 6 mths, Spayed', 'San Marcos Shelter',
                '333 S Twin Oaks Valley Rd, CA, 92096',
                'SanMarcosShelter@FuRiends.co.net', '(512)-252-2543', 'Lola loves to cuddle.', Lola),
          ),
          Visibility(
            visible: sphynx || sanfrancisco,
            child: createAcatCard('cat', 'Leo', '7', '.jpg', 'Leo', 'Sphynx',
                'Male, 1 yrs, Neutered', 'San Francisco',
                '125 Old Pier, CA, 91123',
                'SanFranciscoShelter@FuRiends.co.net', '(415)-112-6531', 'Leo has a big personality.', Leo),
          ),
          Visibility(
            visible: sphynx || sandiego,
            child: createAcatCard('cat', 'Albert', '8', '.jpg', 'Albert', 'Sphynx',
                'Male, 3 yrs, Neutered', 'San Diego Shelter',
                '811 Franklin Street, CA, 92541',
                'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Albert enjoys the finer things in life.', albert),
          ),
          Visibility(
            visible: sphynx || redding,
            child: createAcatCard('cat', 'Louie', '9', '.jpg', 'Louie', 'Sphynx',
                'Male, 2 yrs, Neutered', 'Redding Shelter',
                '798 RedOak Street, CA, 90812',
                'ReddingShelter@FuRiends.co.net', '(530)-653-0096', 'Louie loves other animals', louie),
          ),
          Visibility(
            visible: sphynx || santabarbra,
            child: createAcatCard('cat', 'Cleo', '10', '.jpg', 'Cleo', 'Sphynx',
                'Female, 4 yrs, Spayed', 'Santa Barbra Shelter',
                '900 Transient, CA, 94112',
                'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Cloe loves to play all day.', cleo),
          ),
          Visibility(
            visible: sphynx || sanfrancisco,
            child: createAcatCard('cat', 'Missy', '11', '.jpg', 'Missy', 'Sphynx',
                'Female, 7 yrs, Spayed', 'San Francisco Shelter',
                '125 Old Pier, CA, 91123',
                'SanFranciscoShelter@FuRiends.co.net', '(415)-112-6531', 'Enjoys the comapny of others.', missy),
          ),
          Visibility(
            visible: sphynx || monterey,
            child: createAcatCard('cat', 'Ollie', '12', '.jpg', 'Ollie', 'Sphynx',
                'Male, 4 mths, Not Neutered', 'Monterey Shelter',
                '241 Los Gatos, CA 95521',
                'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Ollie is waiting for his forever home.', ollie),
          ),
          Visibility(
            visible: sphynx || losangeles,
            child: createAcatCard('cat', 'Kali', '13', '.jpg', 'Kali', 'Sphynx',
                'Female, 9 yrs, Spayed', 'Los Angeles Shelter',
                '699 HollyWood Blvd, CA, 97621',
                'LosAngelesShelter@FuRiends.co.net', '(213)-647-6751', 'Kali has a lot of love to give.', kali),
          ),
          Visibility(
            visible: sphynx || losangeles,
            child: createAcatCard('cat', 'Jax', '14', '.jpg', 'Jax', 'Sphynx',
                'Male, 5 months, Not Neutered', 'Los Angeles Shelter',
                '699 HollyWood Blvd, CA, 97621',
                'LosAngelesShelter@FuRiends.co.net', '(213)-647-6751', 'Jax is a little mischievous.', jax),
          ),
          Visibility(
            visible: sphynx || sanmarcos,
            child: createAcatCard('cat', 'Blue', '15', '.jpg', 'Blue', 'Sphynx',
                'Male, 6 yrs, Neutered', 'San Marcos Shelter',
                '333 S Twin Oaks Valley Rd, CA, 92096',
                'SanMarcosShelter@FuRiends.co.net', '(512)-252-2543', 'Blue enjoys cuddling & belly rubs.', blue),
          ),
          Visibility(
            visible: egyptian || monterey,
            child: createAcatCard('cat', 'Rosie', '16', '.jpg', 'Rosie', 'Egyptian Mau',
                'Female, 5 yrs, Spayed', 'Monterey Shelter',
                '241 Los Gatos, CA 95521',
                'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Rosie loves the outdoors', rosie),
          ),
          Visibility(
            visible: egyptian || sandiego,
            child: createAcatCard('cat', 'Gus', '17', '.jpg', 'Gus', 'Egyptian Mau',
                'Male, 7 yrs, Neutered', 'San Diego Shelter',
                '811 Franklin Street, CA, 92541',
                'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Gus loves to look out windows', gus),
          ),
          Visibility(
            visible: egyptian || losangeles,
            child: createAcatCard('cat', 'Ella', '18', '.jpg', 'Ella', 'Egyptian Mau',
                'Female, 8 yrs, Spayed', 'Los Angeles Shelter',
                '699 HollyWood Blvd, CA, 97621',
                'LosAngelesShelter@FuRiends.co.net', '(213)-647-6751', 'Ella is an old girl with lots of energy', ella),
          ),
          Visibility(
            visible: birman || sanmarcos,
            child: createAcatCard('cat', 'Riley', '19', '.jpg', 'Riley', 'Birman',
                'Female, 9 mths, Spayed', 'San Marcos Shelter',
                '333 S Twin Oaks Valley Rd, CA, 92096',
                'SanMarcosShelter@FuRiends.co.net', '(512)-252-2543', 'Riley loves to relax', riley),
          ),
          Visibility(
            visible: birman || redding,
            child: createAcatCard('cat', 'Theo', '20', '.jpg', 'Theo', 'Birman',
                'Male, 8 yrs, Neutered', 'Redding Shelter',
                '798 RedOak Street, CA, 90812',
                'ReddingShelter@FuRiends.co.net', '(530)-653-0096', 'Theo loves company', theo),
          ),
          Visibility(
            visible:  birman || santabarbra,
            child: createAcatCard('cat', 'Tucker', '21', '.jpg', 'Tucker', 'Birman',
                'Male, 7 mths, Neutered', 'Santa Barbra Shelter',
                '900 Transient, CA, 94112',
                'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Tucker is small but mighty.', Tucker),
          ),
        ],
      ),
    );
  }
}