import 'package:finalproject/extraVariables.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'extraVariables.dart';
import 'sideMenu.dart';

class YourAccountInfoPage extends StatefulWidget {
  @override
  _YourAccountInfoPageState createState() => _YourAccountInfoPageState();
}

class _YourAccountInfoPageState extends State<YourAccountInfoPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Your Account Info'),
        backgroundColor: Colors.green[800],
        actions: <Widget>[
        ],
      ),
      body: ListView(
        children: [
          Container(
            height: 140,
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset('images/FinalLogo.png', fit: BoxFit.fill, width: 100, height: 100, ),
              ],
            ),
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 0.5,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.deepPurple[600],
                      Colors.deepPurple[400],
                      Colors.deepPurple[200],
                      Colors.deepPurple[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          ///YOUR CONTACT INFORMATION
          ///-------------------------------------------------------------------
          Row(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Text('YOUR CONTACT INFORMATION',
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.blueGrey[900]))),
                ),
              IconButton(
                icon: Icon(Icons.info_outline, color: Colors.blueGrey[900]),
                tooltip: 'What Does This Mean?',
                onPressed: () {
                  setState(() {
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text('Your Contact Information Will Automatically Be Sent to the Shelter If You Ever Click The Interested Button on an Animal',
                        style: GoogleFonts.zillaSlab(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.lime)),
                      ),
                      duration: Duration(seconds: 4),
                    ));
                    clearAllReptileFilters();
                  });
                },
              ),
            ],
          ),

          Row(
            children: [
              Container(
                  height: 85,
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.label_important, color: Colors.blueGrey[900]),
                          Text(userName,
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.blueGrey[900]))),
                        ],
                      ),
                      Container(height: 5),
                      Row(
                        children: [
                          Icon(Icons.label_important, color: Colors.blueGrey[900]),
                          Text(phoneNumber,
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.blueGrey[900]))),
                        ],
                      ),

                    ],
                  )
              )
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 0.5,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.deepPurple[600],
                      Colors.deepPurple[400],
                      Colors.deepPurple[200],
                      Colors.deepPurple[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          ///VIEW YOUR DONATION HISTORY
          ///-------------------------------------------------------------------
          Row(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Text('YOUR TOTAL DONATIONS',
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.blueGrey[900]))),
              ),

              Container(width: 10),
              Icon(Icons.forward),
              Container(width: 10),

              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Row(
                  children: [
                    Text('\$',
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.green))),

                    Text('$TotalDonated',
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.green))),
                  ],
                )
              ),
            ],
          ),

          Row(
            children: [
              Container(
                  height: 185,
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Please Remember!  Each donation helps',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))),
                      Text('our organization keep animals safe, ',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))),
                      Text('healty, and happy until they find an',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))),
                      Text('amazing owner!  With just a small donation',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))),
                      Text('you could help ensure our animals',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))),
                      Text('find a great home!',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))),
                    ],
                  )
              )
            ],
          ),

          Container(height: 10),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 0.5,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.deepPurple[600],
                      Colors.deepPurple[400],
                      Colors.deepPurple[200],
                      Colors.deepPurple[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

        ],
      ),
    );
  }
}
