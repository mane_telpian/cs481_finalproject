import 'package:finalproject/dogCategory.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'main.dart';
import 'reptilesCategory.dart';
import 'extraVariables.dart';
import 'package:google_fonts/google_fonts.dart';


///Opens a Filter Page to Choose From
///-----------------------------------------------------------------------------
class reptileFilters extends StatefulWidget {
  @override
  _reptileFiltersState createState() => _reptileFiltersState();
}

class _reptileFiltersState extends State<reptileFilters> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void openreptileCategory() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => reptileCategory()));
  }

  bool checkforFilters() {
    if( (snake || turtle || bearded || chameleon) == true )
      showAllREPTILES = false;

    else if( ( snake && turtle && bearded && chameleon ) ==  false )
      showAllREPTILES = true;
  }

  bool checkforFilterLocations() {
    if( (monterey || santabarbra || sanmarcos || sandiego || sanfrancisco || redding || losangeles ) == true )
      showAllREPTILES = false;

    else if( (monterey && santabarbra && sanmarcos && sandiego && sanfrancisco && redding && losangeles ) ==  false )
      showAllREPTILES = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Reptile Filters'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.clear_all),
            color: Colors.white,
            tooltip: 'Clear Filters!',
            onPressed: () {
              setState(() {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Text('Cleared Filters!',
                    style: GoogleFonts.zillaSlab(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.lime)),
                  ),
                  duration: Duration(seconds: 2),
                ));
                clearAllReptileFilters();
              });
            },
          ),

          IconButton(
            icon: Icon(Icons.keyboard_return),
            color: Colors.white,
            tooltip: 'Save Filters!',
            onPressed: () {
              openreptileCategory();
            },
          ),
        ],
        backgroundColor: Colors.green[800],
      ),
      body: ListView(
        children: [
          Row(
            children: [
              Container(
                  height: 50,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(Icons.merge_type),
                      Text('BREEDS', style: TextStyle(fontSize: 20, fontFamily: 'ComicSans', fontWeight: FontWeight.bold, color: Colors.brown)),
                    ],
                  )
              )
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 6,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.brown[600],
                      Colors.brown[400],
                      Colors.brown[200],
                      Colors.brown[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Snake'),
                labelStyle: TextStyle(color: snake ? Colors.black : Colors.black),
                selected: snake,
                onSelected: (bool selected) {
                  setState(() {
                    snake = !snake;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Turtle'),
                labelStyle: TextStyle(color: turtle ? Colors.black : Colors.black),
                selected: turtle,
                onSelected: (bool selected) {
                  setState(() {
                    turtle = !turtle;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Bearded Dragon'),
                labelStyle: TextStyle(color: bearded ? Colors.black : Colors.black),
                selected: bearded,
                onSelected: (bool selected) {
                  setState(() {
                    bearded = !bearded;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Chameleon'),
                labelStyle: TextStyle(color: chameleon ? Colors.black : Colors.black),
                selected: chameleon,
                onSelected: (bool selected) {
                  setState(() {
                    chameleon = !chameleon;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),

          Row(
            children: [
              Container(
                  height: 50,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(Icons.pin_drop),
                      Text('LOCATIONS', style: TextStyle(fontSize: 20, fontFamily: 'ComicSans', fontWeight: FontWeight.bold, color: Colors.brown)),
                    ],
                  )
              )
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 6,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.brown[600],
                      Colors.brown[400],
                      Colors.brown[200],
                      Colors.brown[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Monterey'),
                labelStyle: TextStyle(color: monterey ? Colors.black : Colors.black),
                selected: monterey,
                onSelected: (bool selected) {
                  setState(() {
                    monterey = !monterey;
                    checkforFilterLocations();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('San Diego'),
                labelStyle: TextStyle(color: sandiego ? Colors.black : Colors.black),
                selected: sandiego,
                onSelected: (bool selected) {
                  setState(() {
                    sandiego = !sandiego;
                    checkforFilterLocations();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Santa Barbra'),
                labelStyle: TextStyle(color: santabarbra ? Colors.black : Colors.black),
                selected: santabarbra,
                onSelected: (bool selected) {
                  setState(() {
                    santabarbra = !santabarbra;
                    checkforFilterLocations();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Los Angeles'),
                labelStyle: TextStyle(color: losangeles ? Colors.black : Colors.black),
                selected: losangeles,
                onSelected: (bool selected) {
                  setState(() {
                    losangeles = !losangeles;
                    checkforFilterLocations();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('San Francisco'),
                labelStyle: TextStyle(color: sanfrancisco ? Colors.black : Colors.black),
                selected: sanfrancisco,
                onSelected: (bool selected) {
                  setState(() {
                    sanfrancisco = !sanfrancisco;
                    checkforFilterLocations();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),

          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Redding'),
                labelStyle: TextStyle(color: redding ? Colors.black : Colors.black),
                selected: redding,
                onSelected: (bool selected) {
                  setState(() {
                    redding = !redding;
                    checkforFilterLocations();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('San Marcos'),
                labelStyle: TextStyle(color: sanmarcos ? Colors.black : Colors.black),
                selected: sanmarcos,
                onSelected: (bool selected) {
                  setState(() {
                    sanmarcos = !sanmarcos;
                    checkforFilterLocations();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),

        ],
      ),
    );
  }
}