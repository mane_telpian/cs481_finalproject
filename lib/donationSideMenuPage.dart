import 'package:finalproject/extraVariables.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'extraVariables.dart';
import 'sideMenu.dart';

class DonationPage extends StatefulWidget {
  @override
  _DonationPageState createState() => _DonationPageState();
}

class _DonationPageState extends State<DonationPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool donationdoggoclicked = false;

  void _showBottomSheetCallback(String text){
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return Container(
          decoration: BoxDecoration(
              border: Border(top: BorderSide(color: Colors.black)),
              color: Colors.green[800],
          ),
          child: Padding(
            padding: const EdgeInsets.all(32),
            child: Text(text, textAlign: TextAlign.center,
              style: GoogleFonts.aBeeZee(textStyle: TextStyle(fontSize: 18)),
            ),
          )
      );
    });
  }

  void addDonation(String str) {
    var donationAmount = int.parse(str);
    print(donationAmount);

    ///Add donation amount to TotalDonated for this user
    TotalDonated = TotalDonated + donationAmount;

    print(TotalDonated);
    _showBottomSheetCallback('Thank you for your gracious donation of \$' + '$donationAmount' + '!             ');
  }

  ///Enter Donation Box
  createAlertDialog(BuildContext context) {
    TextEditingController controller = TextEditingController();
    return showDialog(context: context, builder: (context) {
      return AlertDialog(
        title: Text('Enter your Donation Amount!',                           style: GoogleFonts.poppins(
            textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))),
        content: TextField(
          controller: controller,
        ),
        actions: <Widget>[
          MaterialButton(
            elevation: 5.0,
            child: Text('Donate!',                           style: GoogleFonts.poppins(
                textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))),
            onPressed: () {
              setState(() {
                addDonation(controller.text.toString());
                Navigator.of(context).pop();
              });
            },
          )
        ],
      );
    },);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Donate to Us!', style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold), fontSize: 24),),
        backgroundColor: Colors.green[800],
        actions: <Widget>[
        ],
      ),
      body: ListView(
        children: [
          Container(
            height: 160,
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                Image.asset('images/FinalLogo.png', fit: BoxFit.fill, width: 100, height: 100, ),
                Column(
                  children: [
                    Text('"HELP PETS IN NEED"',
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 26, color: Colors.blueGrey[900])
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 0.5,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.blueGrey[900],
                      Colors.blueGrey[900],
                      Colors.blueGrey[900],
                      Colors.blueGrey[900],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          ///WHY DONATE
          ///-------------------------------------------------------------------
          Row(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Text('WHY DONATE ',
                    style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.blueGrey[900]))
                ),
              ),
              Icon(Icons.library_books),
            ],
          ),

          Row(
            children: [
              Container(
                  height: 160,
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Each donation helps our organization',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))
                      ),
                      Text('keep animals safe, healty, and happy',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))
                      ),
                      Text('until they find an amazing owner!  With',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))
                      ),
                      Text('just a small donation, you could help',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))
                      ),
                      Text('ensure our animals find a great home!',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))
                      ),
                    ],
                  )
              )
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 0.5,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.blueGrey[900],
                      Colors.blueGrey[900],
                      Colors.blueGrey[900],
                      Colors.blueGrey[900],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          ///HOW ARE DONATIONS USED AMONG OUR SHELTERS
          ///-------------------------------------------------------------------
          Row(
            children: [
              Container(
                height: 90,
                padding: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('HOW DOES YOUR DONATION',
                            style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.blueGrey[900]))
                        ),
                        Container(width: 10),
                        Icon(Icons.not_listed_location),
                      ],
                    ),
                    Text('BENEFIT EACH SHELTER?',
                        style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.blueGrey[900]))
                    ),
                  ],
                ),
              ),
            ],
          ),

          Row(
            children: [
              Container(
                  height: 130,
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Each donation is spread out across',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))
                      ),
                      Text('all of our loving shelters unless indicated',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))
                      ),
                      Text('by the donator which shelter they intend',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))
                      ),
                      Text('the money to help',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.blueGrey[900]))
                      ),
                    ],
                  )
              ),
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 0.5,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.blueGrey[900],
                      Colors.blueGrey[900],
                      Colors.blueGrey[900],
                      Colors.blueGrey[900],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),


          ///Donate!
          ///-------------------------------------------------------------------
          Row(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('DONATE!',
                        style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.blueGrey[900]))
                    ),
                    Container(width: 10),
                    Icon(Icons.monetization_on),
                  ],
                ),
              ),
            ],
          ),

          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/donationdoggo.jpg'),
                //fit: BoxFit.fill,
              ),
              borderRadius: BorderRadius.circular(20),
            ),
          ),

          Container(height: 20),

          Center(
            child: Container(
              height: 70,
              width: 200,
              child: RaisedButton(
                elevation: 12,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.brown[300])
                ),
                padding: EdgeInsets.all(8.0),
                textColor: Colors.white,
                color: Colors.brown[300],
                onPressed: () {setState(() {
                  createAlertDialog(context);
                });},
                child: Text("Donate Here!", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.white))),
              ),
            ),
          ),

          Container(height: 30),
        ],
      ),
    );
  }
}
