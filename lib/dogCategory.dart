import 'package:finalproject/displayInfoOnAnAnimal.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'main.dart';
import 'sideMenu.dart';
import 'dogFilters.dart';
import 'package:finalproject/extraVariables.dart';
import 'displayInfoOnAnAnimal.dart';
import 'package:google_fonts/google_fonts.dart';
import 'extraVariables.dart';

///Dog Category Page
///Gridlist of Cards for Dogs
///-----------------------------------------------------------------------------
class DogCategory extends StatefulWidget {
  @override
  _DogCategoryState createState() => _DogCategoryState();
}

class _DogCategoryState extends State<DogCategory> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  ///Returns to Explore Page
  ///---------------------------------------------------------------------------
  void openHomePage() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => HomePage()));
  }

  ///Opens Filter Page
  ///---------------------------------------------------------------------------
  void opendogFilters() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => dogFilters()));
  }

  ///Displays Info on the Dog Card Selected
  ///---------------------------------------------------------------------------
  void openDisplayClass(String name, String index, String type, String age,
      String breed, String health, String shelterName, String address,
      String email, String phoneNumber, String description, bool favorited, String imageName) {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => displayInfoForAnimal(
      name: name,
      imageIndex: index,
      imageType: type,
      age: age,
      breed: breed,
      Health: health,
      shelterName: shelterName,
      address: address,
      email: email,
      phoneNumber: phoneNumber,
      description: description,
      FavoritedVariable: favorited,
      imageName: imageName,
    )));
  }

  ///Creates a Card
  ///---------------------------------------------------------------------------
  Card createADogCard(String categoryType, String dogName, String imageIndex,
      String imageType, String age, String breed, String health,
      String shelterName, String address, String email, String phoneNumber, String description, bool favorited) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: GestureDetector(
        onTap: () => openDisplayClass(dogName, imageIndex, imageType, age,
            breed, health, shelterName, address, email, phoneNumber, description, favorited, categoryType),
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/' + categoryType + '$imageIndex' + imageType),
                fit: BoxFit.fill,
              ),
              borderRadius: BorderRadius.all(Radius.circular(20))
          ),
        ),
      ),
    );
  }

  Card test(bool test) {
    print('$test' + 'HERE');

    return Card();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Explore Dogs',
            style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.white))),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.filter_list),
            color: Colors.white,
            tooltip: 'Open Filters for Dogs!',
            onPressed: () => opendogFilters(),
          ),
          IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.white,
            tooltip: 'Return to Explore Page',
            onPressed: () => openHomePage(),
          ),
        ],
        backgroundColor: Colors.green[800],
      ),
      body: showAllDOGS ? ListView(
          children: [
            createADogCard('dog', 'Spot', '0', '.jpg', 'Spot', 'American Bulldog',
                  'Male, 3 mths, Not Neutered', 'Monterey Shelter',
                  '241 Los Gatos, CA, 95521',
                  'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Spot is the most playful boy!', Spot),
            createADogCard('dog', 'Hunk', '1', '.jpg', 'Hunk', 'American Bulldog',
                     'Male, 3 yrs, Neutered', 'Santa Barbara Shelter',
                     '900 Transient, CA, 94112',
                     'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Hunk is a sweet hunk!', Hunk),
            createADogCard('dog', 'Bubba', '2', '.jpg', 'Bubba', 'American Bulldog',
                    'Female, 1 yr, Spayed', 'San Diego Shelter',
                    '811 Franklin Street, CA, 92541',
                    'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Bubba can be shy but loving!', Bubba),
            createADogCard('dog', 'Choco', '4', '.jpg', 'Choco', 'Boston Terrier',
                    'Female, 3 mths, Not Spayed', 'Santa Barbara Shelter',
                    '900 Transient, CA, 94112',
                    'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Choco is as sweet as her name!', Choco),
            createADogCard('dog', 'Boss', '5', '.jpg', 'Boss', 'Boston Terrier',
                    'Male, 6 mths, Neutered', 'Redding Shelter',
                    '798 RedOak Street, CA, 90812',
                    'ReddingShelter@FuRiends.co.net', '(530)-653-0096', 'Boss is extremely energetic!', Boss),
            createADogCard('dog', 'Aki', '6', '.jpeg', 'Aki', 'Husky',
                    'Female, 2 mths, Not Neutered', 'San Diego Shelter',
                    '811 Franklin Street, CA, 92541',
                    'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Aki is a beautiful, playful girl!', Aki),
            createADogCard('dog', 'Lola', '7', '.jpg', 'Lola', 'Chihuahua',
                    'Female, 4 yrs, Spayed', 'Redding Shelter',
                    '798 RedOak Street, CA, 90812',
                    'ReddingShelter@FuRiends.co.net', '(530)-653-0096', 'Lola has a lot of spunk!', Lola),
            createADogCard('dog', 'Foxy', '8', '.jpg', 'Foxy', 'Chihuahua',
                    'Female, 3 yrs, Spayed', 'San Marcos Shelter',
                    '333 S Twin Oaks Valley Rd, CA, 92096',
                    'SanMarcosShelter@FuRiends.co.net', '(512)-252-2543', 'Foxy is sneaky but cute!', Foxy),
            createADogCard('dog', 'Bella', '9', '.jpg', 'Bella', 'Chihuahua',
                    'Female, 1 yr, Spayed', 'San Marcos Shelter',
                    '333 S Twin Oaks Valley Rd, CA, 92096',
                    'SanMarcosShelter@FuRiends.co.net', '(512)-252-2543', 'Bella is waiting for a loving family!', Bella),
            createADogCard('dog', 'Sevan', '10', '.jpg', 'Sevan', 'French Bulldog',
                    'Female, 5 mths, Not Spayed', 'San Fransisco Shelter',
                    '125 Old Pier, CA, 91123',
                    'SanFranciscoShelter@FuRiends.co.net', '(415)-112-6531', 'Sevan is the most energetic girl!', Sevan),
            createADogCard('dog', 'Frankie', '11', '.jpeg', 'Frankie', 'French Bulldog',
                    'Male, 4 mths, Not Neutered', 'San Fransisco Shelter',
                    '125 Old Pier, CA, 91123',
                    'SanFranciscoShelter@FuRiends.co.net', '(415)-112-6531', 'Frankie is the cutest!', Frankie),
            createADogCard('dog', 'Cooper', '12', '.jpeg', 'Cooper', 'Golden Retriever',
                    'Male, 2 yr, Neutered', 'Los Angeles Shelter',
                    '699 HollyWood Blvd, CA, 97621',
                    'LosAngelesShelter@FuRiends.co.net', '(213)-647-6751', 'Cooper is a great boy!', Cooper),
            createADogCard('dog', 'Max', '13', '.jpeg', 'Max', 'Golden Retriever',
                    'Male, 5 yrs, Neutered', 'Los Angeles Shelter',
                    '699 HollyWood Blvd, CA, 97621',
                    'LosAngelesShelter@FuRiends.co.net', '(213)-647-6751', 'Max is faster than youd think!', Max),
            createADogCard('dog', 'Hachi', '14', '.jpg', 'Hachi', 'Husky',
                    'Male, 2 yrs, Neutered', 'San Diego Shelter',
                    '811 Franklin Street, CA, 92541',
                    'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Hachi is the most loyal friend!', Hachi),
            createADogCard('dog', 'Nyla', '15', '.jpg', 'Nyla', 'Husky',
                    'Female, 3 yrs, Spayed', 'Los Angeles Shelter',
                    '699 HollyWood Blvd, CA, 97621',
                    'LosAngelesShelter@FuRiends.co.net', '(213)-647-6751', 'Nyla loves her walks!', Nyla),
            createADogCard('dog', 'Koda', '16', '.jpeg', 'Koda', 'Husky',
                    'Female, 3 yrs, Spayed', 'San Diego Shelter',
                    '811 Franklin Street, CA, 92541',
                    'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Koda is the sweetest!', Koda),
            createADogCard('dog', 'Memphis', '17', '.jpg', 'Memphis', 'Pitbull',
                    'Male, 2 yrs, Neutered', 'San Diego Shelter',
                    '811 Franklin Street, CA, 92541',
                    'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Memphis will protect you!', Memphis),
            createADogCard('dog', 'Prague', '18', '.jpeg', 'Prague', 'Pitbull',
                    'Male, 3 yrs, Neutered', 'Monterey Shelter',
                    '241 Los Gatos, CA, 95521',
                    'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Prague is looking for a loving home!', Prague),
            createADogCard('dog', 'Cronk', '19', '.jpg', 'Cronk', 'Pitbull',
                    'Male, 5 yrs, Neutered', 'Monterey Shelter',
                    '241 Los Gatos, CA, 95521',
                    'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Cronk is not as tough as he looks!', Cronk),
            createADogCard('dog', 'Nola', '20', '.jpg', 'Nola', 'Pitbull',
                    'Female, 3 mths, Not Spayed', 'San Diego Shelter',
                    '811 Franklin Street, CA, 92541',
                    'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Who wouldnt want to adopt Nola!', Nola),
            createADogCard('dog', 'Minnie', '21', '.jpg', 'Minnie', 'Pitbull',
                    'Female, 2 yrs, Spayed', 'Monterey Shelter',
                    '241 Los Gatos, CA, 95521',
                    'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Minnie can be shy!', Minnie),
          ],
        ) : ListView(
        children: [
          Visibility(
            visible: americanbulldog || monterey,
            child: createADogCard('dog', 'Spot', '0', '.jpg', 'Spot', 'American Bulldog',
                'Male, 3 mths, Not Neutered', 'Monterey Shelter',
                '241 Los Gatos, CA, 95521',
                'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Spot is the most playful boy!', Spot),
          ),
          Visibility(
            visible: americanbulldog || santabarbra,
            child: createADogCard('dog', 'Hunk', '1', '.jpg', 'Hunk', 'American Bulldog',
                'Male, 3 yrs, Neutered', 'Santa Barbara Shelter',
                '900 Transient, CA, 94112',
                'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Hunk is a sweet hunk!', Hunk),
          ),
          Visibility(
            visible: americanbulldog || sandiego,
            child: createADogCard('dog', 'Bubba', '2', '.jpg', 'Bubba', 'American Bulldog',
                'Female, 1 yr, Spayed', 'San Diego Shelter',
                '811 Franklin Street, CA, 92541',
                'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Bubba can be shy but loving!', Bubba),
          ),
          Visibility(
            visible: bostonterrier || santabarbra,
            child: createADogCard('dog', 'Choco', '4', '.jpg', 'Choco', 'Boston Terrier',
                'Female, 3 mths, Not Spayed', 'Santa Barbara Shelter',
                '900 Transient, CA, 94112',
                'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Choco is as sweet as her name!', Choco),
          ),
          Visibility(
            visible: bostonterrier || redding,
            child: createADogCard('dog', 'Boss', '5', '.jpg', 'Boss', 'Boston Terrier',
                'Male, 6 mths, Neutered', 'Redding Shelter',
                '798 RedOak Street, CA, 90812',
                'ReddingShelter@FuRiends.co.net', '(530)-653-0096', 'Boss is extremely energetic!', Boss),
          ),
          Visibility(
            visible: husky || sandiego,
            child: createADogCard('dog', 'Aki', '6', '.jpeg', 'Aki', 'Husky',
                'Female, 2 mths, Not Neutered', 'San Diego Shelter',
                '811 Franklin Street, CA, 92541',
                'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Aki is a beautiful, playful girl!', Aki),
          ),
          Visibility(
            visible: chihuahua || redding,
            child: createADogCard('dog', 'Lola', '7', '.jpg', 'Lola', 'Chihuahua',
                'Female, 4 yrs, Spayed', 'Redding Shelter',
                '798 RedOak Street, CA, 90812',
                'ReddingShelter@FuRiends.co.net', '(530)-653-0096', 'Lola has a lot of spunk!', Lola),
          ),
          Visibility(
            visible: chihuahua || sanmarcos,
            child: createADogCard('dog', 'Foxy', '8', '.jpg', 'Foxy', 'Chihuahua',
                'Female, 3 yrs, Spayed', 'San Marcos Shelter',
                '333 S Twin Oaks Valley Rd, CA, 92096',
                'SanMarcosShelter@FuRiends.co.net', '(512)-252-2543', 'Foxy is sneaky but cute!', Foxy),
          ),
          Visibility(
            visible: chihuahua || sanmarcos,
            child: createADogCard('dog', 'Bella', '9', '.jpg', 'Bella', 'Chihuahua',
                'Female, 1 yr, Spayed', 'San Marcos Shelter',
                '333 S Twin Oaks Valley Rd, CA, 92096',
                'SanMarcosShelter@FuRiends.co.net', '(512)-252-2543', 'Bella is waiting for a loving family!', Bella),
          ),
          Visibility(
            visible: frenchbulldog || sanfrancisco,
            child: createADogCard('dog', 'Sevan', '10', '.jpg', 'Sevan', 'French Bulldog',
                'Female, 5 mths, Not Spayed', 'San Fransisco Shelter',
                '125 Old Pier, CA, 91123',
                'SanFranciscoShelter@FuRiends.co.net', '(415)-112-6531', 'Sevan is the most energetic girl!', Sevan),
          ),
          Visibility(
            visible: frenchbulldog || sanfrancisco,
            child: createADogCard('dog', 'Frankie', '11', '.jpeg', 'Frankie', 'French Bulldog',
                'Male, 4 mths, Not Neutered', 'San Fransisco Shelter',
                '125 Old Pier, CA, 91123',
                'SanFranciscoShelter@FuRiends.co.net', '(415)-112-6531', 'Frankie is the cutest!', Frankie),
          ),
          Visibility(
            visible: goldenretriever || losangeles,
            child: createADogCard('dog', 'Cooper', '12', '.jpeg', 'Cooper', 'Golden Retriever',
                'Male, 2 yr, Neutered', 'Los Angeles Shelter',
                '699 HollyWood Blvd, CA, 97621',
                'LosAngelesShelter@FuRiends.co.net', '(213)-647-6751', 'Cooper is a great boy!', Cooper),
          ),
          Visibility(
            visible: goldenretriever || losangeles,
            child: createADogCard('dog', 'Max', '13', '.jpeg', 'Max', 'Golden Retriever',
                'Male, 5 yrs, Neutered', 'Los Angeles Shelter',
                '699 HollyWood Blvd, CA, 97621',
                'LosAngelesShelter@FuRiends.co.net', '(213)-647-6751', 'Max is faster than youd think!', Max),
          ),
          Visibility(
            visible: husky || sandiego,
            child: createADogCard('dog', 'Hachi', '14', '.jpg', 'Hachi', 'Husky',
                'Male, 2 yrs, Neutered', 'San Diego Shelter',
                '811 Franklin Street, CA, 92541',
                'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Hachi is the most loyal friend!', Hachi),
          ),
          Visibility(
            visible: husky || losangeles,
            child: createADogCard('dog', 'Nyla', '15', '.jpg', 'Nyla', 'Husky',
                'Female, 3 yrs, Spayed', 'Los Angeles Shelter',
                '699 HollyWood Blvd, CA, 97621',
                'LosAngelesShelter@FuRiends.co.net', '(213)-647-6751', 'Nyla loves her walks!', Nyla),
          ),
          Visibility(
            visible: husky || sandiego,
            child: createADogCard('dog', 'Koda', '16', '.jpeg', 'Koda', 'Husky',
                'Female, 3 yrs, Spayed', 'San Diego Shelter',
                '811 Franklin Street, CA, 92541',
                'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Koda is the sweetest!', Koda),
          ),
          Visibility(
            visible: pitbull || sandiego,
            child: createADogCard('dog', 'Memphis', '17', '.jpg', 'Memphis', 'Pitbull',
                'Male, 2 yrs, Neutered', 'San Diego Shelter',
                '811 Franklin Street, CA, 92541',
                'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Memphis will protect you!', Memphis),
          ),
          Visibility(
            visible: pitbull || monterey,
            child: createADogCard('dog', 'Prague', '18', '.jpeg', 'Prague', 'Pitbull',
                'Male, 3 yrs, Neutered', 'Monterey Shelter',
                '241 Los Gatos, CA, 95521',
                'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Prague is looking for a loving home!', Prague),
          ),
          Visibility(
            visible: pitbull || monterey,
            child: createADogCard('dog', 'Cronk', '19', '.jpg', 'Cronk', 'Pitbull',
                'Male, 5 yrs, Neutered', 'Monterey Shelter',
                '241 Los Gatos, CA, 95521',
                'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Cronk is not as tough as he looks!', Cronk),
          ),
          Visibility(
            visible: pitbull || sandiego,
            child: createADogCard('dog', 'Nola', '20', '.jpg', 'Nola', 'Pitbull',
                'Female, 3 mths, Not Spayed', 'San Diego Shelter',
                '811 Franklin Street, CA, 92541',
                'SanDiegoShelter@FuRiends.co.net', '(858)-678-6890', 'Who wouldnt want to adopt Nola!', Nola),
          ),
          Visibility(
            visible: pitbull || monterey,
            child: createADogCard('dog', 'Minnie', '21', '.jpg', 'Minnie', 'Pitbull',
                'Female, 2 yrs, Spayed', 'Monterey Shelter',
                '241 Los Gatos, CA, 95521',
                'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Minnie can be shy!', Minnie),
          ),
        ],
      ),
    );
  }
}