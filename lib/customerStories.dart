import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'main.dart';
import 'package:google_fonts/google_fonts.dart';
import 'sideMenu.dart';

class customerReviews extends StatefulWidget {
  @override
  _customerReviewsState createState() => _customerReviewsState();
}

class _customerReviewsState extends State<customerReviews> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  ///Returns to Explore Page
  ///---------------------------------------------------------------------------
  void openHomePage() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => HomePage()));
  }

  Container createACusomterReview(String customerName, String dogName, String image, String line1, String line2) {
    return Container(
      child: Column(
        children: [
          Container(height: 20),

          Text(customerName + ' & ' + dogName,
            style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 32, fontWeight: FontWeight.bold, color: Colors.blueGrey[900])),
          ),

          Container(height: 20),

          Container(
            height: 200,
            width: 250,
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/' + image),
                  fit: BoxFit.fill,
                ),
                borderRadius: BorderRadius.all(Radius.circular(20))
            ),
          ),

          Container(height: 20),

          Text(line1,
            style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w400, color: Colors.blueGrey[900])),
          ),


          Text(line2,
            style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w400, color: Colors.blueGrey[900])),
          ),


          Container(height: 20),

          Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: Container(
                height: 2,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.blueGrey[900],
                      Colors.blueGrey[900],
                      Colors.blueGrey[900],
                      Colors.blueGrey[900],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Customer Stories'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.white,
            tooltip: 'Return to Explore Page',
            onPressed: () => openHomePage(),
          ),
        ],
        backgroundColor: Colors.green[800],
      ),
      body: ListView(
        children: [
          ///CUSTOMER REVIEWS
          ///-------------------------------------------------------------------
          createACusomterReview('Haley', 'Mittens', 'story1.jpg', '"Adoption process was a breeze & ', 'FuRiends made it easier to find my sweet Mittens"'),
          createACusomterReview('Harrison', 'Buck', 'story3.jpg', '"Buck was the perfect Co-Star in my new movie', '  "The Call of the Wild. Thanks FuRiends!'),
          createACusomterReview('Karen', 'Maximus', 'story2.jpg', '"For the first time ever I was truly happy with a', 'pet app. Recommend FuRiends to all my ladies!"'),
          createACusomterReview('Taylor', 'Marley', 'story4.jpg', '"Love my new bestie! Totes in love with Marley.', 'Couldn\'t have asked for a better FuRiend!"'),
          createACusomterReview('Debbie', 'Alaska', 'story5.jpg', '"Love you FuRiends! Thank you for helping', 'me find my adventure buddy!"'),
          createACusomterReview('Alex', 'Thumper', 'story6.jpg', '"Thumper loves to sleep as much as I do!', 'He\'s my new Thunder Buddy!"'),

          Container(height: 30),
        ],
      ),


    );
  }
}
