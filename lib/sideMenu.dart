import 'package:finalproject/extraVariables.dart';
import 'package:flutter/material.dart';
import 'main.dart';
import 'donationSideMenuPage.dart';
import 'yourAccountInfoSideMenuPage.dart';

///SideMenu Class
///-----------------------------------------------------------------------------
class SideMenu extends StatefulWidget {
  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void openHomePage() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => HomePage()));
  }

  void opendonationPage() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => DonationPage()));
  }

  void openyourAccountPage() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => YourAccountInfoPage()));
  }


  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Colors.white,
              Colors.white,
              Colors.white,
              Colors.white,
              Colors.white,
            ],
            stops: [0.1, 0.3, 0.5, 0.7, 0.9],
          ),
        ),
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage('images/FinalLogo.png'))),
              child: Container(),
            ),

            ListTile(
              leading: Icon(Icons.home),
              title: Text('Home'),
              onTap: () {openHomePage();},
            ),

            ListTile(
              leading: Icon(Icons.monetization_on),
              title: Text('Donate to Us'),
              onTap: () {opendonationPage();},
            ),


            ListTile(
              leading: Icon(Icons.account_box),
              title: Text('Your Account Info'),
              onTap: () {openyourAccountPage();},
            ),
          ],
        ),
      ),
    );
  }
}