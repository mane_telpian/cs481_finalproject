int TotalDonated = 0;
String phoneNumber = '';
String userName = '';
bool userNameSaved = false;
bool phoneNumberSaved = false;
bool allowLogin = false;
bool showCircularProgressBar = false;
bool loading = false;

///Show ALl Bool Variables
bool showAllDOGS = true;
bool showAllCATS = true;
bool showAllREPTILES = true;
bool showAllSMALLANIMALS = true;

///Locations
bool monterey = false;
bool sandiego = false;
bool santabarbra = false;
bool losangeles = false;
bool sanfrancisco = false;
bool redding = false;
bool sanmarcos = false;

///Favorited Dogs
bool Spot = false;
bool Hunk = false;
bool Bubba = false;
bool Choco = false;
bool Boss = false;
bool Aki = false;
bool Lola_dog = false;
bool Foxy = false;
bool Bella_dog = false;
bool Sevan = false;
bool Frankie = false;
bool Cooper = false;
bool Max = false;
bool Hachi = false;
bool Nyla = false;
bool Koda = false;
bool Memphis = false;
bool Prague = false;
bool Cronk = false;
bool Nola = false;
bool Minnie = false;

///Favorited Cats
bool Charlie = false;
bool Bella = false;
bool Jack = false;
bool Sammy = false;
bool Chloe = false;
bool Lola = false;
bool Leo = false;
bool albert = false;
bool louie = false;
bool cleo = false;
bool missy = false;
bool ollie = false;
bool kali = false;
bool jax = false;
bool blue = false;
bool rosie = false;
bool gus = false;
bool ella = false;
bool riley = false;
bool theo = false;
bool Tucker = false;

///Favorited Reptiles
bool Rambo = false;
bool Spike = false;
bool Medusa = false;
bool Cactus = false;
bool Rex = false;
bool Trippy = false;
bool Pixie = false;
bool Leafy = false;
bool Pogo = false;
bool Slytherin = false;
bool Severus = false;
bool Kaa = false;
bool Snappy = false;
bool Michelangelo = false;
bool Mercutio = false;

///Favorited Small Animals
bool buttons = false;
bool spark = false;
bool baja = false;
bool sol = false;
bool ginger = false;
bool smallblue = false;
bool pandora = false;
bool gizmo = false;
bool gucci = false;
bool splinter = false;
bool buster = false;
bool jojo = false;
bool yoshi = false;
bool peach = false;
bool cosmo = false;
bool ren = false;
bool bugs = false;
bool thumper = false;
bool lili = false;
bool rojo = false;

///Dogs
bool husky = false;
bool pitbull = false;
bool goldenretriever = false;
bool americanbulldog = false;
bool chihuahua = false;
bool bostonterrier = false;
bool frenchbulldog = false;

///Cats
bool persian = false;
bool siamese = false;
bool sphynx = false;
bool birman = false;
bool egyptian = false;

///Reptiles
bool bearded = false;
bool chameleon = false;
bool snake = false;
bool turtle = false;

///SmallAnimals
bool rabbit = false;
bool mice = false;
bool hamster = false;
bool guinea = false;
bool chinchillas = false;

void updateBooleanValue(bool boolValuetoUpdate) {
  if(boolValuetoUpdate == false)
    boolValuetoUpdate = true;

  else
    boolValuetoUpdate = false;
}

void clearAllLocations() {
  monterey = false;
  sandiego = false;
  santabarbra = false;
  losangeles = false;
  sanfrancisco = false;
  redding = false;
  sanmarcos = false;
}

void clearAllDogFilters() {
  showAllDOGS = true;

  husky = false;
  pitbull = false;
  goldenretriever = false;
  americanbulldog = false;
  chihuahua = false;
  bostonterrier = false;
  frenchbulldog = false;

  clearAllLocations();
}

void clearAllCatFilters() {
  showAllCATS = true;

  persian = false;
  siamese = false;
  sphynx = false;
  egyptian = false;
  birman = false;

  clearAllLocations();
}

void clearAllReptileFilters() {
  showAllREPTILES = true;

  bearded = false;
  chameleon = false;
  snake = false;
  turtle = false;

  clearAllLocations();
}

void clearALlSmallAnimalFilters() {
  showAllSMALLANIMALS = true;

  rabbit = false;
  mice = false;
  hamster = false;
  guinea = false;
  chinchillas = false;

  clearAllLocations();
}