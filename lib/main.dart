import 'package:finalproject/catCategory.dart';
import 'package:finalproject/customerStories.dart';
import 'package:finalproject/dogBreedInformation.dart';
import 'package:finalproject/reptilesCategory.dart';
import 'package:finalproject/smallanimalsCategory.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dogCategory.dart';
import 'sideMenu.dart';
import 'openingLogo.dart';
import 'extraVariables.dart';
import 'package:animations/animations.dart';
import 'displayInfoOnAnAnimal.dart';

void main() {runApp(MyApp());}

///MyApp
///Root Widget of our Final Project Application
///-----------------------------------------------------------------------------
class MyApp extends StatelessWidget {
  @override

  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Final Project',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        backgroundColor: Colors.white,
        body: LogoApp(),
      ),
    );
  }
}

///HomePage
///Creates a Bottom Navigation Bar which allows the user to switch between pages
///on the app screen
///-----------------------------------------------------------------------------
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  List<Widget> _screens = [ ExplorePage(), ExploreBreeds() ];
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: PageTransitionSwitcher(
        duration: Duration(milliseconds: 1200),
        transitionBuilder: (
            child,
            animation,
            secondaryAnimation,
            ) {
          return FadeThroughTransition(
            fillColor: Colors.white,
            animation: animation,
            secondaryAnimation: secondaryAnimation,
            child: child,
          );
        },
        child: _screens[_selectedIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.green[800],
        currentIndex: _selectedIndex,
        onTap: (_Index) {
          setState(() {
            _selectedIndex = _Index;
          });},
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.explore, color: _selectedIndex == 0 ? Colors.grey[400] : Colors.white,),
            title: Text('Explore', style: TextStyle(color: _selectedIndex == 0 ? Colors.grey[400] : Colors.white)),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.library_books, color: _selectedIndex == 1 ? Colors.grey[400] : Colors.white,),
            title: Text('Learn', style: TextStyle(color: _selectedIndex == 1 ? Colors.grey[400] : Colors.white,)),
          ),
        ],
      ),
    );
  }
}

///ExplorePage
///First Screen the user sees after logging in
///-----------------------------------------------------------------------------
class ExplorePage extends StatefulWidget {
  @override
  _ExplorePageState createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  ///Category Changes
  ///---------------------------------------------------------------------------
  void movetoDogCategory() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => DogCategory()));
  }

  void movetoCatsCategory() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => catCategory()));
  }

  void movetoReptilesCategory() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => reptileCategory()));
  }

  void movetoSmallAnimalsCategory() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => smallanimalCategory()));
  }

  void openExploreBreeds() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => ExploreBreeds()));
  }

  void movetoCustomerReviews() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => customerReviews()));
  }

  void _showBottomSheetCallback(String text){
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return Container(
          decoration: BoxDecoration(
            border: Border(top: BorderSide(color: Colors.black)),
            color: Colors.green[800]
          ),
          child: Padding(
            padding: const EdgeInsets.all(32),
            child: Text(text, textAlign: TextAlign.center,
              style: GoogleFonts.aBeeZee(textStyle: TextStyle(fontSize: 18)),
            ),
          )
      );
    });
  }

  ///Explore Page Functions
  ///---------------------------------------------------------------------------
  void determineCategory(String categoryName) {
    print('$Spot');

    if(categoryName == 'dogcover')
      movetoDogCategory();

    if(categoryName == 'catcover')
      movetoCatsCategory();

    if(categoryName == 'reptilecover')
      movetoReptilesCategory();

    if(categoryName == 'smallanimalscover')
      movetoSmallAnimalsCategory();
  }

  ///Displays Info on The New Animal Card Selected
  ///---------------------------------------------------------------------------
  void openDisplayClass(String name, String index, String type, String age,
      String breed, String health, String shelterName, String address,
      String email, String phoneNumber, String description, bool favorited, String imageName) {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => displayInfoForAnimal(
      name: name,
      imageIndex: index,
      imageType: type,
      age: age,
      breed: breed,
      Health: health,
      shelterName: shelterName,
      address: address,
      email: email,
      phoneNumber: phoneNumber,
      description: description,
      FavoritedVariable: favorited,
      imageName: imageName,
    )));
  }

  ///Creates a New Animal Card
  ///---------------------------------------------------------------------------
  Card createANewAnimalCard(String categoryType, String dogName, String imageIndex,
      String imageType, String age, String breed, String health,
      String shelterName, String address, String email, String phoneNumber, String description, bool favorited) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: GestureDetector(
        onTap: () => openDisplayClass(dogName, imageIndex, imageType, age,
            breed, health, shelterName, address, email, phoneNumber, description, favorited, categoryType),
        child: Container(
          height: 150,
          width: 150,
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/' + categoryType + '$imageIndex' + imageType),
                fit: BoxFit.fill,
              ),
              borderRadius: BorderRadius.all(Radius.circular(20))
          ),
        ),
      ),
    );
  }

  Card Category(String categoryName, String photoType) {
    return Card(
      elevation: 12,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: GestureDetector(
        onTap: () {
          determineCategory(categoryName);
        },
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/' + categoryName + photoType),
                fit: BoxFit.fill,
              ),
              borderRadius: BorderRadius.all(Radius.circular(20))
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: SideMenu(),
        key: _scaffoldKey,
        appBar: AppBar(
        title: Text('Explore',
          style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold), fontSize: 24),
        ),
        backgroundColor: Colors.green[800],
          actions: <Widget>[
          ],
      ),
      body: ListView(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Text('New Animals',
                    style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.blueGrey[900])),
                  ),
                ),

              IconButton(
                icon: Icon(Icons.question_answer),
                color: Colors.brown[300],
                tooltip: 'View Info About How FuRiends Finds New Animals!',
                onPressed: () => _showBottomSheetCallback('New Animals are those in need of a welcoming & friendly home!'),
              ),
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: Container(
                height: 0.7,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.grey[500],
                      Colors.grey[500],
                      Colors.grey[500],
                      Colors.grey[500],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          Container(height: 10),

          Container(
            height: 100,
            width: 100,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                createANewAnimalCard('dog', 'Foxy', '8', '.jpg', 'Foxy', 'Chihuahua',
                    'Female, 3 yrs, Spayed', 'San Marcos Shelter',
                    '333 S Twin Oaks Valley Rd, CA, 92096',
                    'SanMarcosShelter@FuRiends.co.net', '(512)-252-2543', 'Foxy is sneaky but cute!', Foxy),
                createANewAnimalCard('dog', 'Cooper', '12', '.jpeg', 'Cooper', 'Golden Retriever',
                    'Male, 2 yr, Neutered', 'Los Angeles Shelter',
                    '699 HollyWood Blvd, CA, 97621',
                    'LosAngelesShelter@FuRiends.co.net', '(213)-647-6751', 'Cooper is a great boy!', Cooper),
                createANewAnimalCard('cat', 'Missy', '11', '.jpg', 'Missy', 'Sphynx',
                    'Female, 7 yrs, Spayed', 'San Francisco Shelter',
                    '125 Old Pier, CA, 91123',
                    'SanFranciscoShelter@FuRiends.co.net', '(415)-112-6531', 'Enjoys the company of others', missy),
                createANewAnimalCard('cat', 'Tucker', '21', '.jpg', 'Tucker', 'Birman',
                    'Male, 7 mths, Neutered', 'Santa Barbra Shelter',
                    '900 Transient, CA, 94112',
                    'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Tucker is small but mighty.', Tucker),
                createANewAnimalCard('reptile', 'Rex', '5', '.jpg', 'Rex', 'Bearded Dragon',
                    'Male, 4 mths', 'San Fransisco Shelter',
                    '125 Old Pier, CA 91123',
                    'SanFransisco@FuRiends.co.net', '(415)-112-6531', 'Rex may be small, but he is trouble', Rex),
                createANewAnimalCard('reptile', 'Kaa', '12', '.jpg', 'Kaa', 'Snake',
                    'Female, 2 yrs', 'San Fransisco Shelter',
                    '125 Old Pier, CA 91123',
                    'SanFransisco@FuRiends.co.net', '(415)-112-6531', 'Kaa is as graceful as she is loving!', Kaa),
                createANewAnimalCard('cat', 'Tucker', '21', '.jpg', 'Tucker', 'Birman',
                    'Male, 7 mths, Neutered', 'Santa Barbra Shelter',
                    '900 Transient, CA, 94112',
                    'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Tucker is small but mighty.', Tucker),
                createANewAnimalCard('smallanimal', 'Blue', '7', '.jpg', 'Blue', 'Guinea Pig',
                    'Male, 5 mths', 'Monterey Shelter',
                    '241 Los Gatos, CA 95521',
                    ' MontereyShelter@FuRiends.co.net', '(831) - 051 - 1589', 'Loves to play and jump around.', blue),
                createANewAnimalCard('smallanimal', 'Peach', '17', '.jpg', 'Peach', 'Mice',
                    'Female, 2 yrs, 3 mths, fits', 'San Diego Shelter',
                    '811 Franklin Street, CA 92541',
                    'SanDiegoShelter@FuRiends.co.net', '(858) - 678 - 6890', 'A princess. Demands attention.', peach),

              ],
            ),
          ),

          Container(height: 10),

          Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: Container(
                height: 0.7,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.grey[500],
                      Colors.grey[500],
                      Colors.grey[500],
                      Colors.grey[500],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          ///CATEGORIES
          ///-------------------------------------------------------------------
          GridView.count(
            shrinkWrap: true,
            primary: false,
            padding: const EdgeInsets.all(20),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 2,
            physics: ScrollPhysics(),
            children: <Widget>[
              Category('dogcover', '.jpg'),
              Category('catcover', '.png'),
              Category('reptilecover', '.jpg'),
              Category('smallanimalscover', '.jpg'),
            ],
          ),

          ///CUSTOMER REVIEWS
          ///-------------------------------------------------------------------
          Container(height: 30),

          Center(
            child: Container(
              height: 70,
              width: 200,
              child: RaisedButton(
                elevation: 12,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.brown[300])
                ),
                padding: EdgeInsets.all(8.0),
                textColor: Colors.white,
                color: Colors.brown[300],
                onPressed: () {setState(() {
                  movetoCustomerReviews();
                });},
                child: Text("Read Customer's Stories!",                           style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 14, color: Colors.white))),
              ),
            ),
          ),

          Container(height: 30),

        ],
      ),
    );
  }
}

class ExploreBreeds extends StatefulWidget {
  @override
  _ExploreBreedsState createState() => _ExploreBreedsState();
}

class _ExploreBreedsState extends State<ExploreBreeds> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  ///Category Changes
  ///---------------------------------------------------------------------------
  void movetodogBreedInformationPage(String name, String imageType, String imageIndex,
      String age, String health, String breed, String description) {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => dogBreedInformationPage(
      name: name,
      imageType: imageType,
      imageIndex: imageIndex,
      age: age,
      Health: health,
      breed: breed,
      description: description,
    )));
  }

  void movetoCatsCategory() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => catCategory()));
  }

  void movetoReptilesCategory() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => reptileCategory()));
  }

  void movetoSmallAnimalsCategory() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => smallanimalCategory()));
  }

  Card createABreadCard(String name, String imageType, String imageIndex,
      String age, String health, String breed, String description) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: GestureDetector(
        onTap: () {
          movetodogBreedInformationPage(name, imageType, imageIndex, age, health, breed, description);
        },
        child: Container(
              height: 175,
              width: 175,
              decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/' + name + imageIndex + imageType),
                    fit: BoxFit.fill,
                  ),
              ),
          child: Align(
              alignment: Alignment.bottomRight,
              child: Text(breed,
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white)),
              ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Learn More About Breeds',
    style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Colors.white))),
        backgroundColor: Colors.green[800],
        actions: [

        ],
      ),
      body: ListView(
        children: [
          ///DOG BREEDS
          ///-------------------------------------------------------------------
          Row(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Text('Dog Breeds',
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.blueGrey[900])),
                ),
              ),
            ],
          ),

          Container(
            height: 120,
            width: 150,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                createABreadCard('dog', '.jpeg', '6', '11-13', 'Generally Healthy', 'Husky', 'Friendly, Adventurous, Loving'),
                createABreadCard('dog', '.jpeg', '13', '10-12', 'CHD, Skin Problems, Lymphoma', 'Golden Retriever', 'Playful, Loyal, Energetic'),
                createABreadCard('dog', '.jpg', '1', '10-15', 'Kidney and Thyroid disorders', 'American Bulldog', 'Confident, Energetic, Active'),
                createABreadCard('dog', '.jpg', '9', '14-18', 'Generally Healthy', 'Chihuahua', 'Graceful, Small, Compact'),
                createABreadCard('dog', '.jpg', '21', '8-15', 'Generally Healthy', 'Pitbull', 'Stubborn, Intelligent'),
                createABreadCard('dog', '.jpg', '5', '10-14', 'Generally Healthy', 'Boston Terrier', 'Devoted, Clever, Fast Learner'),
                createABreadCard('dog', '.jpg', '10', '10-14', 'CHD, Allergies, Stenotic nares','French Bulldog', 'Entertaining, Sweet, Cute'),
              ],
            ),
          ),

          ///CAT BREEDS
          ///-------------------------------------------------------------------
          Row(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Text('Cat Breeds',
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.blueGrey[900])),
              ),
    ),
            ],
          ),

          Container(
            height: 120,
            width: 150,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                createABreadCard('cat', '.jpg', '0', '10-17', 'Bladder Infections, PKD, PRA, HCM', 'Persian', 'Quiet and Sweet'),
                createABreadCard('cat', '.jpg', '5', '10-12', 'Gum Disease, Urinary Tract Disease', 'Siamese', 'Highly Intelligent, Talkative'),
                createABreadCard('cat', '.jpg', '10', '8-14', 'HCM, Myopathy', 'Sphynx', 'Outgoing and Mischievous'),
                createABreadCard('cat', '.jpg', '18', '15', 'Heart Disease, Asthma', 'Egyptian Mau', 'Gentle & reserved'),
                createABreadCard('cat', '.jpg', '20', '15', 'Kidney Disease', 'Birman', 'Loving & Affectionate'),
              ],
            ),
          ),

          ///REPTILE BREEDS
          ///-------------------------------------------------------------------
          Row(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Text('Reptile Breeds',
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.blueGrey[900])),
              ),
    ),
            ],
          ),

          Container(
            height: 120,
            width: 150,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                createABreadCard('reptile', '.jpg', '0', '10-15', 'Metabolic bone disease, Mouth Rot', 'Bearded Dragon', 'Generally docile'),
                createABreadCard('reptile', '.jpg', '6', '7-10', 'Can be stressed easily', 'Chameleon', 'Solitary animals'),
                createABreadCard('reptile', '.jpg', '10', '7-9', 'Stomatitis, Parasites', 'Snake', 'Solitary animals.'),
                createABreadCard('reptile', '.jpg', '14', '30', 'Salmonella, Shell fractures', 'Turtle', 'Mild temperament, Loves food'),
              ],
            ),
          ),

          ///SMALLANIMAL BREEDS
          ///-------------------------------------------------------------------
          Row(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Text('Small Animal Breeds',
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.blueGrey[900])),
              ),
              ),
            ],
          ),

          Container(
            height: 120,
            width: 150,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                createABreadCard('smallanimal', '.jpg', '20', '8-12', 'Hairballs, Snuffles, Overgrown Teeth', 'Rabbits', 'Love to dig burrows for nesting.'),
                createABreadCard('smallanimal', '.jpg', '17', '2-3', 'Anorexia, overgrown teeth, tumours.', 'Mice', 'Can be social over time. Best paired.'),
                createABreadCard('smallanimal', '.jpg', '10', '2-3', 'Teeth problems, hair loss', 'Hamsters', 'Nocturnal animals. Active at night.'),
                createABreadCard('smallanimal', '.jpg', '5', '5-7', 'Respiratory diseases, scurvy.', 'Guinea Pigs', 'Like to take naps and relax its head.'),
                createABreadCard('smallanimal', '.jpg', '1', '5-8', 'Overgrown teeth, diarrhea', 'Chinchillas', 'Best paired with other chinchillas'),
              ],
            ),
          ),


        ],
      ),
    );
  }
}