import 'package:finalproject/dogCategory.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'main.dart';
import 'smallanimalsCategory.dart';
import 'extraVariables.dart';
import 'package:google_fonts/google_fonts.dart';

///Opens a Filter Page to Choose From
///-----------------------------------------------------------------------------
class smallanimalFilters extends StatefulWidget {
  @override
  _smallanimalFiltersState createState() => _smallanimalFiltersState();
}

class _smallanimalFiltersState extends State<smallanimalFilters> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void opensmallanimalsCategory() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => smallanimalCategory()));
  }

  bool checkforFilters() {
    if( (rabbit || mice || hamster || guinea || chinchillas) == true )
      showAllSMALLANIMALS = false;

    else if( ( rabbit && mice && hamster && guinea && chinchillas ) ==  false )
      showAllSMALLANIMALS = true;
  }

  bool checkforFilterLocations() {
    if( (monterey || santabarbra || sanmarcos || sandiego || sanfrancisco || redding || losangeles ) == true )
      showAllSMALLANIMALS = false;

    else if( (monterey && santabarbra && sanmarcos && sandiego && sanfrancisco && redding && losangeles ) ==  false )
      showAllSMALLANIMALS = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Small Animal Filters'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.clear_all),
            color: Colors.white,
            tooltip: 'Clear Filters!',
            onPressed: () {
              setState(() {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Text('Cleared Filters!',
                    style: GoogleFonts.zillaSlab(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.lime)),
                  ),
                  duration: Duration(seconds: 2),
                ));
                clearALlSmallAnimalFilters();
              });
            },
          ),

          IconButton(
            icon: Icon(Icons.keyboard_return),
            color: Colors.white,
            tooltip: 'Save Filters!',
            onPressed: () {
              opensmallanimalsCategory();
            },
          ),
        ],
        backgroundColor: Colors.green[800],
      ),
      body: ListView(
        children: [
          Row(
            children: [
              Container(
                  height: 50,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(Icons.merge_type),
                      Text('BREEDS', style: TextStyle(fontSize: 20, fontFamily: 'ComicSans', fontWeight: FontWeight.bold, color: Colors.brown)),
                    ],
                  )
              )
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 6,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.brown[600],
                      Colors.brown[400],
                      Colors.brown[200],
                      Colors.brown[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Rabbits'),
                labelStyle: TextStyle(color: rabbit ? Colors.black : Colors.black),
                selected: rabbit,
                onSelected: (bool selected) {
                  setState(() {
                    rabbit = !rabbit;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Mice'),
                labelStyle: TextStyle(color: mice ? Colors.black : Colors.black),
                selected: mice,
                onSelected: (bool selected) {
                  setState(() {
                    mice = !mice;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Hamsters'),
                labelStyle: TextStyle(color: hamster ? Colors.black : Colors.black),
                selected: hamster,
                onSelected: (bool selected) {
                  setState(() {
                    hamster = !hamster;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Guinea Pigs'),
                labelStyle: TextStyle(color: guinea ? Colors.black : Colors.black),
                selected: guinea,
                onSelected: (bool selected) {
                  setState(() {
                    guinea= !guinea;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Chinchillas'),
                labelStyle: TextStyle(color: chinchillas ? Colors.black : Colors.black),
                selected: chinchillas,
                onSelected: (bool selected) {
                  setState(() {
                    chinchillas = !chinchillas;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),

          Row(
            children: [
              Container(
                  height: 50,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(Icons.pin_drop),
                      Text('LOCATIONS', style: TextStyle(fontSize: 20, fontFamily: 'ComicSans', fontWeight: FontWeight.bold, color: Colors.brown)),
                    ],
                  )
              )
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 6,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.brown[600],
                      Colors.brown[400],
                      Colors.brown[200],
                      Colors.brown[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Monterey'),
                labelStyle: TextStyle(color: monterey ? Colors.black : Colors.black),
                selected: monterey,
                onSelected: (bool selected) {
                  setState(() {
                    monterey = !monterey;
                    checkforFilterLocations();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('San Diego'),
                labelStyle: TextStyle(color: sandiego ? Colors.black : Colors.black),
                selected: sandiego,
                onSelected: (bool selected) {
                  setState(() {
                    sandiego = !sandiego;
                    checkforFilterLocations();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Santa Barbra'),
                labelStyle: TextStyle(color: santabarbra ? Colors.black : Colors.black),
                selected: santabarbra,
                onSelected: (bool selected) {
                  setState(() {
                    santabarbra = !santabarbra;
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Los Angeles'),
                labelStyle: TextStyle(color: losangeles ? Colors.black : Colors.black),
                selected: losangeles,
                onSelected: (bool selected) {
                  setState(() {
                    losangeles = !losangeles;
                    checkforFilterLocations();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('San Francisco'),
                labelStyle: TextStyle(color: sanfrancisco ? Colors.black : Colors.black),
                selected: sanfrancisco,
                onSelected: (bool selected) {
                  setState(() {
                    sanfrancisco = !sanfrancisco;
                    checkforFilterLocations();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),

          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Redding'),
                labelStyle: TextStyle(color: redding ? Colors.black : Colors.black),
                selected: redding,
                onSelected: (bool selected) {
                  setState(() {
                    redding = !redding;
                    checkforFilterLocations();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('San Marcos'),
                labelStyle: TextStyle(color: sanmarcos ? Colors.black : Colors.black),
                selected: sanmarcos,
                onSelected: (bool selected) {
                  setState(() {
                    sanmarcos = !sanmarcos;
                    checkforFilterLocations();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),

        ],
      ),
    );
  }
}