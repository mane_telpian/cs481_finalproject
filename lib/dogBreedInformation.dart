import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'main.dart';
import 'sideMenu.dart';

class dogBreedInformationPage extends StatefulWidget {
  dogBreedInformationPage({this.name, this.imageType, this.imageIndex,
    this.age, this.Health, this.breed, this.description});
  final String name;
  final String imageIndex;
  final String imageType;
  final String age;
  final String Health;
  final String breed;
  final String description;

  @override
  _dogBreedInformationPageState createState() => _dogBreedInformationPageState();
}

class _dogBreedInformationPageState extends State<dogBreedInformationPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void openHomePage() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => ExploreBreeds()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: SideMenu(),
      appBar: AppBar(
        title: Text(widget.breed, style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.white)),),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.white,
            tooltip: 'Return to Breeds Info Page',
            onPressed: () => openHomePage(),
          ),
        ],
        backgroundColor: Colors.green[800],
      ),
      body: ListView(
        children: <Widget>[
          Container(
            height: 200,
            width: 200,
            child: Container(
              height: 200,
              width: 200,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/' + widget.name + widget.imageIndex + widget.imageType),
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),

          Container(
            height: 80,
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(widget.breed,
              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 32, fontWeight: FontWeight.bold, color: Colors.blueGrey[900]))),
              ],
            ),
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 0.5,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.deepPurple[600],
                      Colors.deepPurple[400],
                      Colors.deepPurple[200],
                      Colors.deepPurple[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          Row(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Text('Average Lifespan', style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.blueGrey[900]))),
              ),
            ],
          ),

          Row(
            children: [
              Container(
                  height: 50,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(Icons.library_books),
                      Container(width: 5),
                      Text(widget.age + ' Years', style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w400, color: Colors.blueGrey[900]))),
                    ],
                  )
              )
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 0.5,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.deepPurple[600],
                      Colors.deepPurple[400],
                      Colors.deepPurple[200],
                      Colors.deepPurple[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          Row(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Text('Health', style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.blueGrey[900]))),
              )
            ],
          ),

          Row(
            children: [
              Container(
                  height: 50,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(Icons.healing),
                      Container(width: 5),
                      Text(widget.Health, style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w400, color: Colors.blueGrey[900]))),
                    ],
                  )
              )
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 0.5,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.deepPurple[600],
                      Colors.deepPurple[400],
                      Colors.deepPurple[200],
                      Colors.deepPurple[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          Row(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(10),
                child: Text('Description', style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.blueGrey[900]))),
              )
            ],
          ),

          Row(
            children: [
              Container(
                  height: 50,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(Icons.textsms),
                      Container(width: 5),
                      Text(widget.description, style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w400, color: Colors.blueGrey[900]))),
                    ],
                  )
              )
            ],
          ),


        ],
      ),
    );
  }
}
