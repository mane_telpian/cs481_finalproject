import 'package:finalproject/reptileFilters.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'main.dart';
import 'sideMenu.dart';
import 'extraVariables.dart';
import 'reptileFilters.dart';
import 'displayInfoOnAnAnimal.dart';
import 'package:google_fonts/google_fonts.dart';

///reptile Category Page
///Gridlist of Cards for reptiles
///-----------------------------------------------------------------------------
class reptileCategory extends StatefulWidget {
  @override
  _reptileCategoryState createState() => _reptileCategoryState();
}

class _reptileCategoryState extends State<reptileCategory> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  ///Returns to Explore Page
  ///---------------------------------------------------------------------------
  void openHomePage() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => HomePage()));
  }

  ///Opens Filter Page
  ///---------------------------------------------------------------------------
  void openreptileFilters() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => reptileFilters()));
  }

  ///Displays Info on the reptile Card Selected
  ///---------------------------------------------------------------------------
  void openDisplayClass(String name, String index, String type, String age,
      String breed, String health, String shelterName, String address,
      String email, String phoneNumber, String description, bool favorited, String imageName) {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => displayInfoForAnimal(
      name: name,
      imageIndex: index,
      imageType: type,
      age: age,
      breed: breed,
      Health: health,
      shelterName: shelterName,
      address: address,
      email: email,
      phoneNumber: phoneNumber,
      description: description,
      FavoritedVariable: favorited,
      imageName: imageName,
    )));
  }

  ///Creates a Card
  ///---------------------------------------------------------------------------
  Card createAreptileCard(String categoryType, String reptileName, String imageIndex,
      String imageType, String age, String breed, String health,
      String shelterName, String address, String email, String phoneNumber, String description,
      bool favorited) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: GestureDetector(
        onTap: () => openDisplayClass(reptileName, imageIndex, imageType, age,
            breed, health, shelterName, address, email, phoneNumber, description, favorited, categoryType),
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/' + categoryType + '$imageIndex' + imageType),
                fit: BoxFit.fill,
              ),
              borderRadius: BorderRadius.all(Radius.circular(20))
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Explore Reptiles',
            style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.white))),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.filter_list),
            color: Colors.white,
            tooltip: 'Open Filters for Reptiles!',
            onPressed: () => openreptileFilters(),
          ),
          IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.white,
            tooltip: 'Return to Explore Page',
            onPressed: () => openHomePage(),
          ),
        ],
        backgroundColor: Colors.green[800],
      ),
      body: showAllREPTILES ? ListView(
        children: [
          createAreptileCard('reptile', 'Rambo', '0', '.jpg', 'Rambo', 'Bearded Dragon',
                'Male, 2 yrs', 'Monterey Shelter',
                '241 Los Gatos, CA, 95521',
                'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Rambo is an energetic friend!', Rambo),
          createAreptileCard('reptile', 'Spike', '1', '.jpg', 'Spike', 'Bearded Dragon',
                'Male, 4 yrs, Metabolic bone disease', 'Santa Barbara Shelter',
                '900 Transient, CA 94112',
                'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Spike is looking for a loving family!', Spike),
          createAreptileCard('reptile', 'Medusa', '2', '.jpg', 'Medusa', 'Bearded Dragon',
              'Female, 2 yrs', 'San Diego Shelter',
              '811 Franklin Street, CA 92541',
              'SanDiego@FuRiends.co.net', '(858)-678-6890', 'Our girl Medusa has a big appetite!', Medusa),
          createAreptileCard('reptile', 'Cactus', '4', '.jpg', 'Cactus', 'Bearded Dragon',
              'Male, 1 yr', 'Los Angeles Shelter',
              '699 HollyWood Blvd, CA 97621',
              'LosAngeles@FuRiends.co.net', '(213)-647-6751', 'Cactus is a spiky fellow!', Cactus),
          createAreptileCard('reptile', 'Rex', '5', '.jpg', 'Rex', 'Bearded Dragon',
              'Male, 4 mths', 'San Fransisco Shelter',
              '125 Old Pier, CA 91123',
              'SanFransisco@FuRiends.co.net', '(415)-112-6531', 'Rex may be small, but he is trouble!', Rex),
          createAreptileCard('reptile', 'Trippy', '6', '.jpg', 'Trippy', 'Chameleon',
              'Female, 5 yrs', 'Redding Shelter',
              '798 RedOak Street, CA 90812',
              'ReddingShelter@FuRiends.co.net', '(530)-653-0096', 'Trippy will surely brighten your life!', Trippy),
          createAreptileCard('reptile', 'Pixie', '7', '.jpg', 'Pixie', 'Chameleon',
              'Female, 1 yrs', 'San Marcos Shelter',
              '333 S Twin Oaks Valley Rd, CA 92096',
              'SanMarcosShelter@FuRiends.co.net', '(512)-653-2543', 'Meet Pixie, our sweet friend!', Pixie),
          createAreptileCard('reptile', 'Leafy', '8', '.jpg', 'Leafy', 'Chameleon',
              'Female, 3 yrs', 'Monterey Shelter',
              '241 Los Gatos, CA, 95521',
              'MontereyShelter@co.monterey.net', '(831)-051-1589', 'Leafy is looking for a loving home!', Leafy),
          createAreptileCard('reptile', 'Pogo', '9', '.jpg', 'Pogo', 'Chameleon',
              'Male, 5 yrs', 'Santa Barbara Shelter',
              '900 Transient, CA 94112',
              'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Pogo is a jumpy yet friendly!', Pogo),
          createAreptileCard('reptile', 'Slytherin', '10', '.jpg', 'Slytherin', 'Snake',
              'Male, 3 yrs', 'San Diego Shelter',
              '811 Franklin Street, CA 92541',
              'SanDiego@FuRiends.co.net', '(858)-678-6890', 'Slytherin promises he wont bite!', Slytherin),
          createAreptileCard('reptile', 'Severus', '11', '.jpg', 'Severus', 'Snake',
              'Male, 4 yrs, Infectious stomatitis', 'Los Angeles Shelter',
              '699 HollyWood Blvd, CA 97621',
              'LosAngeles@FuRiends.co.net', '(213)-647-6751', 'Severus sometimes needs space!', Severus),
          createAreptileCard('reptile', 'Kaa', '12', '.jpg', 'Kaa', 'Snake',
              'Female, 2 yrs', 'San Fransisco Shelter',
              '125 Old Pier, CA 91123',
              'SanFransisco@FuRiends.co.net', '(415)-112-6531', 'Kaa is as graceful as she is loving!', Kaa),
          createAreptileCard('reptile', 'Snappy', '13', '.jpg', 'Snappy', 'Turtle',
              'Male, 5 mths', 'Redding Shelter',
              '798 RedOak Street, CA 90812',
              'ReddingShelter@FuRiends.co.net', '(530)-653-0096', 'Snappy gets sassy sometimes', Snappy),
          createAreptileCard('reptile', 'Michelangelo', '14', '.jpg', 'Michelangelo', 'Turtle',
              'Male, 1 yr', 'San Marcos Shelter',
              '333 S Twin Oaks Valley Rd, CA 92096',
              'SanMarcosShelter@FuRiends.co.net', '(512)-653-2543', 'Michelangelo needs a loving home!', Michelangelo),
          createAreptileCard('reptile', 'Mercutio', '15', '.jpg', 'Mercutio', 'Turtle',
              'Male, 9 mths, shell infection', 'Monterey Shelter',
              '241 Los Gatos, CA, 95521',
              'MontereyShelter@co.monterey.net', '(831)-051-1589', 'Mercutio is the greatest companion', Mercutio),
        ],
      ) : ListView(
        children: [
          Visibility(
            visible: bearded || monterey,
            child: createAreptileCard('reptile', 'Rambo', '0', '.jpg', 'Rambo', 'Bearded Dragon',
                'Male, 2 yrs', 'Monterey Shelter',
                '241 Los Gatos, CA, 95521',
                'MontereyShelter@FuRiends.co.net', '(831)-051-1589', 'Rambo is an energetic friend!', Rambo),
          ),
          Visibility(
            visible: bearded || santabarbra,
            child: createAreptileCard('reptile', 'Spike', '1', '.jpg', 'Spike', 'Bearded Dragon',
                'Male, 4 yrs', 'Santa Barbara Shelter',
                '900 Transient, CA 94112',
                'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Spike is looking for a loving family!', Spike),
          ),
          Visibility(
            visible: bearded || sandiego,
            child: createAreptileCard('reptile', 'Medusa', '2', '.jpg', 'Medusa', 'Bearded Dragon',
                'Female, 2 yrs', 'San Diego Shelter',
                '811 Franklin Street, CA 92541',
                'SanDiego@FuRiends.co.net', '(858)-678-6890', 'Our girl Medusa has a big appetite!', Medusa),
          ),
          Visibility(
            visible: bearded || losangeles,
            child: createAreptileCard('reptile', 'Cactus', '4', '.jpg', 'Cactus', 'Bearded Dragon',
                'Male, 1 yr', 'Los Angeles Shelter',
                '699 HollyWood Blvd, CA 97621',
                'LosAngeles@FuRiends.co.net', '(213)-647-6751', 'Cactus is a spiky fellow!', Cactus),
          ),
          Visibility(
            visible: bearded || sanfrancisco,
            child: createAreptileCard('reptile', 'Rex', '5', '.jpg', 'Rex', 'Bearded Dragon',
                'Male, 4 mths', 'San Fransisco Shelter',
                '125 Old Pier, CA 91123',
                'SanFransisco@FuRiends.co.net', '(415)-112-6531', 'Rex may be small, but he is trouble!', Rex),
          ),
          Visibility(
            visible: chameleon || redding,
            child: createAreptileCard('reptile', 'Trippy', '6', '.jpg', 'Trippy', 'Chameleon',
                'Female, 5 yrs', 'Redding Shelter',
                '798 RedOak Street, CA 90812',
                'ReddingShelter@FuRiends.co.net', '(530)-653-0096', 'Trippy will surely brighten your life!', Trippy),
          ),
          Visibility(
            visible: chameleon || sanfrancisco,
            child: createAreptileCard('reptile', 'Pixie', '7', '.jpg', 'Pixie', 'Chameleon',
                'Female, 1 yrs', 'San Marcos Shelter',
                '333 S Twin Oaks Valley Rd, CA 92096',
                'SanMarcosShelter@FuRiends.co.net', '(512)-653-2543', 'Meet Pixie, our sweet friend!', Pixie),
          ),
          Visibility(
            visible: chameleon || monterey,
            child: createAreptileCard('reptile', 'Leafy', '8', '.jpg', 'Leafy', 'Chameleon',
                'Female, 3 yrs', 'Monterey Shelter',
                '241 Los Gatos, CA, 95521',
                'MontereyShelter@co.monterey.net', '(831)-051-1589', 'Leafy is looking for a loving home!', Leafy),
          ),
          Visibility(
            visible: chameleon || santabarbra,
            child: createAreptileCard('reptile', 'Pogo', '9', '.jpg', 'Pogo', 'Chameleon',
                'Male, 5 yrs', 'Santa Barbara Shelter',
                '900 Transient, CA 94112',
                'SantaBarbaraShelter@FuRiends.co.net', '(805)-689-6731', 'Pogo is a jumpy yet friendly!', Pogo),
          ),
          Visibility(
            visible:  snake || sandiego,
            child: createAreptileCard('reptile', 'Slytherin', '10', '.jpg', 'Slytherin', 'Snake',
                'Male, 3 yrs', 'San Diego Shelter',
                '811 Franklin Street, CA 92541',
                'SanDiego@FuRiends.co.net', '(858)-678-6890', 'Slytherin promises he wont bite!', Slytherin),
          ),
          Visibility(
            visible:  snake || losangeles,
            child: createAreptileCard('reptile', 'Severus', '11', '.jpg', 'Severus', 'Snake',
                'Male, 4 yrs', 'Los Angeles Shelter',
                '699 HollyWood Blvd, CA 97621',
                'LosAngeles@FuRiends.co.net', '(213)-647-6751', 'Severus sometimes needs space!', Severus),
          ),
          Visibility(
            visible:  snake || sanfrancisco,
            child: createAreptileCard('reptile', 'Kaa', '12', '.jpg', 'Kaa', 'Snake',
                'Female, 2 yrs', 'San Fransisco Shelter',
                '125 Old Pier, CA 91123',
                'SanFransisco@FuRiends.co.net', '(415)-112-6531', 'Kaa is as graceful as she is loving!', Kaa),
          ),
          Visibility(
            visible: turtle || redding,
            child: createAreptileCard('reptile', 'Snappy', '13', '.jpg', 'Snappy', 'Turtle',
                'Male, 5 mths', 'Redding Shelter',
                '798 RedOak Street, CA 90812',
                'ReddingShelter@FuRiends.co.net', '(530)-653-0096', 'Snappy gets sassy sometimes', Snappy),
          ),
          Visibility(
            visible: turtle || sanmarcos,
            child: createAreptileCard('reptile', 'Michelangelo', '14', '.jpg', 'Michelangelo', 'Turtle',
                'Male, 1 yr', 'San Marcos Shelter',
                '333 S Twin Oaks Valley Rd, CA 92096',
                'SanMarcosShelter@FuRiends.co.net', '(512)-653-2543', 'Michelangelo needs a loving home!', Michelangelo),
          ),
          Visibility(
            visible: turtle || monterey,
            child: createAreptileCard('reptile', 'Mercutio', '15', '.jpg', 'Mercutio', 'Turtle',
                'Male, 9 mths, shell infection', 'Monterey Shelter',
                '241 Los Gatos, CA, 95521',
                'MontereyShelter@co.monterey.net', '(831)-051-1589', 'Mercutio is the greatest companion', Mercutio),
          ),
        ],
      ),
    );
  }
}