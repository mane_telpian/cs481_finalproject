import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'main.dart';
import 'sideMenu.dart';
import 'smallanimalsFilters.dart';
import 'extraVariables.dart';
import 'displayInfoOnAnAnimal.dart';
import 'package:google_fonts/google_fonts.dart';

///smallanimal Category Page
///Gridlist of Cards for smallanimals
///-----------------------------------------------------------------------------
class smallanimalCategory extends StatefulWidget {
  @override
  _smallanimalCategoryState createState() => _smallanimalCategoryState();
}

class _smallanimalCategoryState extends State<smallanimalCategory> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  ///Returns to Explore Page
  ///---------------------------------------------------------------------------
  void openHomePage() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => HomePage()));
  }

  ///Opens Filter Page
  ///---------------------------------------------------------------------------
  void opensmallanimalsFilters() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => smallanimalFilters()));
  }

  ///Displays Info on the smallanimal Card Selected
  ///---------------------------------------------------------------------------
  void openDisplayClass(String name, String index, String type, String age,
      String breed, String health, String shelterName, String address,
      String email, String phoneNumber, String description, bool favorited, String imageName) {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => displayInfoForAnimal(
      name: name,
      imageIndex: index,
      imageType: type,
      age: age,
      breed: breed,
      Health: health,
      shelterName: shelterName,
      address: address,
      email: email,
      phoneNumber: phoneNumber,
      description: description,
      FavoritedVariable: favorited,
      imageName: imageName,
    )));
  }

  ///Creates a Card
  ///---------------------------------------------------------------------------
  Card createAsmallanimalCard(String categoryType, String smallanimalName, String imageIndex,
      String imageType, String age, String breed, String health,
      String shelterName, String address, String email, String phoneNumber, String description, bool favorited) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: GestureDetector(
        onTap: () => openDisplayClass(smallanimalName, imageIndex, imageType, age,
            breed, health, shelterName, address, email, phoneNumber, description, favorited, categoryType),
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/' + categoryType + '$imageIndex' + imageType),
                fit: BoxFit.fill,
              ),
              borderRadius: BorderRadius.all(Radius.circular(20))
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Explore Small Animals',
            style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white))),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.filter_list),
            color: Colors.white,
            tooltip: 'Open Filters for Small Animals!',
            onPressed: () => opensmallanimalsFilters(),
          ),
          IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.white,
            tooltip: 'Return to Explore Page!',
            onPressed: () => openHomePage(),
          ),
        ],
        backgroundColor: Colors.green[800],
      ),
      body: showAllSMALLANIMALS ? ListView(
        children: [
          createAsmallanimalCard('smallanimal', 'Buttons', '0', '.jpg', 'Buttons', 'Chinchilla',
                'Male, 3 mths', 'Santa Barbara Shelter',
                '900 Transient Street, CA 94112',
                'SantaBarbaraShelter@FuRiends.co.net', '(805) - 689 - 6731', 'Young, feisty, and full of energy.', buttons),
          createAsmallanimalCard('smallanimal', 'Spark', '1', '.jpg', 'Spark', 'Chinchilla',
              'Male, 6 mths', 'San Diego Shelter',
              '811 Franklin Street, CA 92541',
              'SanDiegoShelter@FuRiends.co.net', '(858) - 678 - 6890', 'Reserved, but warms up fast.', spark),
          createAsmallanimalCard('smallanimal', 'Baja', '4', '.jpg', 'Baja', 'Chinchilla',
              'Female, 2 mths', 'San Francisco Shelter',
              '125 Old Pier, CA 91123',
              'SanFranciscoShelter@FuRiends.co.net', '(415) - 112 - 6531', 'Fluffy and loves to cuddle.', baja),
          createAsmallanimalCard('smallanimal', 'Sol', '5', '.jpg', 'Sol', 'Chinchilla',
              'Female, 6 mths', 'San Marcos Shelter',
              '333 S Twin Oaks Valley Rd, CA 92096',
              'SanMarcosShelter@FuRiends.co.net', '(512) - 252 - 2543', 'Soft and gentle.', sol),
          createAsmallanimalCard('smallanimal', 'Ginger', '6', '.jpg', 'Ginger', 'Guinea Pig',
              'Female, 7 mths', 'Redding Shelter',
              '798 RedOak Street, CA 90812',
              'ReddingShelter@FuRiends.co.net', '(530) -653 - 0096', 'A lovable oaf.', ginger),
          createAsmallanimalCard('smallanimal', 'Blue', '7', '.jpg', 'Blue', 'Guinea Pig',
              'Male, 5 mths', 'Monterey Shelter',
              '241 Los Gatos, CA 95521',
              ' MontereyShelter@FuRiends.co.net', '(831) - 051 - 1589', 'Loves to play and jump around.', blue),
          createAsmallanimalCard('smallanimal', 'Pandora', '8', '.jpg', 'Pandora', 'Guinea Pig',
              'Female, 1 yr, 9 mths', 'San Diego Shelter',
              '811 Franklin Street, CA 92541',
              'SanDiegoShelter@FuRiends.co.net', '(858) - 678 - 6890', 'Energetic little angel with spunk.', pandora),
          createAsmallanimalCard('smallanimal', 'Gizmo', '9', '.jpg', 'Gizmo', 'Guinea Pig',
              'Male, 1 yr, 3 mths', 'Los Angeles Shelter',
              '699 HollyWood Blvd, CA 97621',
              'LosAngelesShelter@FuRiends.co.net', '(213) - 647 - 6751', 'Loves to play and full of energy.', gizmo),
          createAsmallanimalCard('smallanimal', 'Gucci', '11', '.jpg', 'Gucci', 'Hamster',
              'Female, 11 mths', 'Monterey Shelter',
              '241 Los Gatos, CA 95521',
              'MontereyShelter@FuRiends.co.net', '(831) - 051 - 1589', 'Loves to cuddle and sleep.', gucci),
          createAsmallanimalCard('smallanimal', 'Splinter', '12', '.jpg', 'Splinter', 'Hamster',
              'Male, 2 yrs, 1 mth', 'Los Angeles Shelter',
              '699 HollyWood Blvd, CA 97621',
              'LosAngelesShelter@FuRiends.co.net', '(213) - 647 - 6751', 'Very energetic and wild.', splinter),
          createAsmallanimalCard('smallanimal', 'Buster', '13', '.jpg', 'Buster', 'Hamster',
              'Male, 1 yr, 3 mths', 'Redding Shelter',
              '798 RedOak Street, CA 90812',
              'ReddingShelter@FuRiends.co.net', '(530) -653 - 0096', 'Loves to play and get dirty.', buster),
          createAsmallanimalCard('smallanimal', 'Jojo', '14', '.jpg', 'Jojo', 'Hamster',
              'Male, 11 mths', 'San Diego Shelter',
              '811 Franklin Street, CA 92541',
              'SanDiegoShelter@FuRiends.co.net', '(858) - 678 - 6890', 'Loves to play with other hamsters.', jojo),
          createAsmallanimalCard('smallanimal', 'Yoshi', '16', '.jpg', 'Yoshi', 'Mice',
              'Male, 1 yr, 3 mths', 'Santa Barbra Shelter',
              '900 Transient, CA 94112',
              'SantaBarbaraShelter@FuRiends.co.net', '(805) - 689 - 6731', 'Loves to jump around.', yoshi),
          createAsmallanimalCard('smallanimal', 'Peach', '17', '.jpg', 'Peach', 'Mice',
              'Female, 2 yrs, 3 mths, fits', 'San Diego Shelter',
              '811 Franklin Street, CA 92541',
              'SanDiegoShelter@FuRiends.co.net', '(858) - 678 - 6890', 'A princess. Demands attention.', peach),
          createAsmallanimalCard('smallanimal', 'Cosmo', '18', '.jpg', 'Cosmo', 'Mice',
              'Male, 3 yrs, 2 mths', 'San Francisco Shelter',
              '125 Old Pier, CA 91123',
              'SanFranciscoShelter@FuRiends.co.net', '(415) - 112 - 6531', 'Loves to cuddle.', cosmo),
          createAsmallanimalCard('smallanimal', 'Ren', '19', '.jpg', 'Ren', 'Mice',
              'Male, 2 yrs, 5 mths, sore nose', 'Santa Barbra Shelter',
              '900 Transient, CA 94112',
              'SantaBarbaraShelter@FuRiends.co.net', '(805) - 689 - 6731', 'A mischievous, lovable punk.', ren),
          createAsmallanimalCard('smallanimal', 'Bugs', '21', '.jpg', 'Bugs', 'Rabbit',
              'Male, 2 yrs, 1 mth', 'San Marcos Shelter',
              '333 S Twin Oaks Valley Rd, CA, 92096',
              'SanMarcosShelter@FuRiends.co.net', '(512) - 252 - 2543', 'Plays around with other animals.', bugs),
          createAsmallanimalCard('smallanimal', 'Thumper', '22', '.jpg', 'Thumper', 'Rabbit',
              'Male, 2 yrs, 4 mths, overgrown teeth', 'Redding Shelter',
              '798 RedOak Street, CA, 90812',
              'SanMarcosShelter@FuRiends.co.net', '(530) -653 - 0096', 'Gets with the rhythm, man.', thumper),
          createAsmallanimalCard('smallanimal', 'Lili', '23', '.jpg', 'Lili', 'Rabbit',
              'Female, 2 yrs, 1 mth, hairballs', 'San Francisco Shelter',
              '125 Old Pier, CA, 91123',
              'SanFranciscoShelter@FuRiends.co.net', '(415) - 112 - 6531', 'Fluffy hair and loves to cuddle.', lili),
          createAsmallanimalCard('smallanimal', 'Rojo', '24', '.jpg', 'Rojo', 'Rabbit',
              'Male, 4 yrs, Overgrown teeth', 'San Marcos Shelter',
              '333 S Twin Oaks Valley Rd, CA, 92096',
              'SanMarcosShelter@FuRiends.co.net', '(512) - 252 - 2543', 'Energetic & competitive with others', rojo),
        ],
      ) : ListView(
        children: [
          Visibility(
            visible: chinchillas || santabarbra,
            child: createAsmallanimalCard('smallanimal', 'Buttons', '0', '.jpg', 'Buttons', 'Chinchilla',
                'Male, 3 mths', 'Santa Barbara Shelter',
                '900 Transient Street, CA 94112',
                'SantaBarbaraShelter@FuRiends.co.net', '(805) - 689 - 6731', 'Young, feisty, and full of energy. ', buttons),
          ),
          Visibility(
            visible: chinchillas || sandiego,
            child: createAsmallanimalCard('smallanimal', 'Spark', '1', '.jpg', 'Spark', 'Chinchilla',
                'Male, 6 mths', 'San Diego Shelter',
                '811 Franklin Street, CA 92541',
                'SanDiegoShelter@FuRiends.co.net', '(858) - 678 - 6890', 'Reserved, but warms up fast.', spark),
          ),
          Visibility(
            visible: chinchillas || sanfrancisco,
            child: createAsmallanimalCard('smallanimal', 'Baja', '4', '.jpg', 'Baja', 'Chinchilla',
                'Female, 2 mths', 'San Francisco Shelter',
                '125 Old Pier, CA 91123',
                'SanFranciscoShelter@FuRiends.co.net', '(415) - 112 - 6531', 'Fluffy and loves to cuddle.', baja),
          ),
          Visibility(
            visible: chinchillas || sanmarcos,
            child: createAsmallanimalCard('smallanimal', 'Sol', '5', '.jpg', 'Sol', 'Chinchilla',
                'Female, 6 mths', 'San Marcos Shelter',
                '333 S Twin Oaks Valley Rd, CA 92096',
                'SanMarcosShelter@FuRiends.co.net', '(512) - 252 - 2543', 'Soft and gentle.', sol),
          ),
          Visibility(
            visible: guinea || redding,
            child: createAsmallanimalCard('smallanimal', 'Ginger', '6', '.jpg', 'Ginger', 'Guinea Pig',
                'Female, 7 mths', 'Redding Shelter',
                '798 RedOak Street, CA 90812',
                'ReddingShelter@FuRiends.co.net', '(530) -653 - 0096', 'A lovable oaf.', ginger),
          ),
          Visibility(
            visible: guinea || monterey,
            child: createAsmallanimalCard('smallanimal', 'Blue', '7', '.jpg', 'Blue', 'Guinea Pig',
                'Male, 5 mths', 'Monterey Shelter',
                '241 Los Gatos, CA 95521',
                ' MontereyShelter@FuRiends.co.net', '(831) - 051 - 1589', 'Loves to play and jump around.', smallblue),
          ),
          Visibility(
            visible: guinea || sandiego,
            child: createAsmallanimalCard('smallanimal', 'Pandora', '8', '.jpg', 'Pandora', 'Guinea Pig',
                'Female, 1 yr, 9 mths', 'San Diego Shelter',
                '811 Franklin Street, CA 92541',
                'SanDiegoShelter@FuRiends.co.net', '(858) - 678 - 6890', 'Energetic little angel with spunk.', pandora),
          ),
          Visibility(
            visible: guinea || losangeles,
            child: createAsmallanimalCard('smallanimal', 'Gizmo', '9', '.jpg', 'Gizmo', 'Guinea Pig',
                'Male, 1 yr, 3 mths', 'Los Angeles Shelter',
                '699 HollyWood Blvd, CA 97621',
                'LosAngelesShelter@FuRiends.co.net', '(213) - 647 - 6751', 'Loves to play and full of energy.', gizmo),
          ),
          Visibility(
            visible: hamster || monterey,
            child: createAsmallanimalCard('smallanimal', 'Gucci', '11', '.jpg', 'Gucci', 'Hamster',
                'Female, 11 mths', 'Monterey Shelter',
                '241 Los Gatos, CA 95521',
                'MontereyShelter@FuRiends.co.net', '(831) - 051 - 1589', 'Loves to cuddle and sleep.', gucci),
          ),
          Visibility(
            visible: hamster || losangeles,
            child: createAsmallanimalCard('smallanimal', 'Splinter', '12', '.jpg', 'Splinter', 'Hamster',
                'Male, 2 yrs, 1 mth', 'Los Angeles Shelter',
                '699 HollyWood Blvd, CA 97621',
                'LosAngelesShelter@FuRiends.co.net', '(213) - 647 - 6751', 'Very energetic and wild.', splinter),
          ),
          Visibility(
            visible:  hamster || redding,
            child: createAsmallanimalCard('smallanimal', 'Buster', '13', '.jpg', 'Buster', 'Hamster',
                'Male, 1 yr, 3 mths', 'Redding Shelter',
                '798 RedOak Street, CA 90812',
                'ReddingShelter@FuRiends.co.net', '(530) -653 - 0096', 'Loves to play and get dirty.', buster),
          ),
          Visibility(
            visible: hamster || sandiego,
            child: createAsmallanimalCard('smallanimal', 'Jojo', '14', '.jpg', 'Jojo', 'Hamster',
                'Male, 11 mths', 'San Diego Shelter',
                '811 Franklin Street, CA 92541',
                'SanDiegoShelter@FuRiends.co.net', '(858) - 678 - 6890', 'Loves to play with other hamsters.', jojo),
          ),
          Visibility(
            visible: mice || santabarbra,
            child: createAsmallanimalCard('smallanimal', 'Yoshi', '16', '.jpg', 'Yoshi', 'Mice',
                'Male, 1 yr, 3 mths', 'Santa Barbra Shelter',
                '900 Transient, CA 94112',
                'SantaBarbaraShelter@FuRiends.co.net', '(805) - 689 - 6731', 'Loves to jump off around.', yoshi),
          ),
          Visibility(
            visible: mice || sandiego,
            child: createAsmallanimalCard('smallanimal', 'Peach', '17', '.jpg', 'Peach', 'Mice',
                'Female, 2 yrs, 3 mths, fits', 'San Diego Shelter',
                '811 Franklin Street, CA 92541',
                'SanDiegoShelter@FuRiends.co.net', '(858) - 678 - 6890', 'A princess. Demands attention.', peach),
          ),
          Visibility(
            visible: mice || sanfrancisco,
            child: createAsmallanimalCard('smallanimal', 'Cosmo', '18', '.jpg', 'Cosmo', 'Mice',
                'Male, 3 yrs, 2 mths', 'San Francisco Shelter',
                '125 Old Pier, CA 91123',
                'SanFranciscoShelter@FuRiends.co.net', '(415) - 112 - 6531', 'Loves to cuddle.', cosmo),
          ),
          Visibility(
            visible: mice || santabarbra,
            child: createAsmallanimalCard('smallanimal', 'Ren', '19', '.jpg', 'Ren', 'Mice',
                'Male, 2 yrs, 5 mths, sore nose', 'Santa Barbra Shelter',
                '900 Transient, CA 94112',
                'SantaBarbaraShelter@FuRiends.co.net', '(805) - 689 - 6731', 'A mischievous, lovable punk.', ren),
          ),
          Visibility(
            visible: rabbit || sanmarcos,
            child: createAsmallanimalCard('smallanimal', 'Bugs', '21', '.jpg', 'Bugs', 'Rabbit',
                'Male, 2 yrs, 1 mth', 'San Marcos Shelter',
                '333 S Twin Oaks Valley Rd, CA, 92096',
                'SanMarcosShelter@FuRiends.co.net', '(512) - 252 - 2543', 'Plays around with other animals.', bugs),
          ),
          Visibility(
            visible:  rabbit || redding,
            child: createAsmallanimalCard('smallanimal', 'Thumper', '22', '.jpg', 'Thumper', 'Rabbit',
                'Male, 2 yrs, 4 mths, overgrown teeth', 'Redding Shelter',
                '798 RedOak Street, CA, 90812',
                'SanMarcosShelter@FuRiends.co.net', '(530) -653 - 0096', 'Gets with the rhythm, man.', thumper),
          ),
          Visibility(
            visible: rabbit || sanfrancisco,
            child: createAsmallanimalCard('smallanimal', 'Lili', '23', '.jpg', 'Lili', 'Rabbit',
                'Female, 2 yrs, 1 mth, hairballs', 'San Francisco Shelter',
                '125 Old Pier, CA, 91123',
                'SanFranciscoShelter@FuRiends.co.net', '(415) - 112 - 6531', 'Fluffy hair and loves to cuddle.', lili),
          ),
          Visibility(
            visible: rabbit || sanmarcos,
            child: createAsmallanimalCard('smallanimal', 'Rojo', '24', '.jpg', 'Rojo', 'Rabbit',
                'Male, 4 yrs, Overgrown teeth', 'San Marcos Shelter',
                '333 S Twin Oaks Valley Rd, CA, 92096',
                'SanMarcosShelter@FuRiends.co.net', '(512) - 252 - 2543', 'Energetic & competitive with others', rojo),
          ),
        ],
      ),
    );
  }
}