import 'package:finalproject/dogCategory.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'extraVariables.dart';
import 'package:google_fonts/google_fonts.dart';

///Opens a Filter Page to Choose From
///-----------------------------------------------------------------------------
class dogFilters extends StatefulWidget {
  @override
  _dogFiltersState createState() => _dogFiltersState();
}

class _dogFiltersState extends State<dogFilters> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void opendogCategory() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => DogCategory()));
  }

  bool checkforFilters() {
    if( (husky || frenchbulldog || goldenretriever || pitbull || americanbulldog || chihuahua || bostonterrier) == true )
      showAllDOGS = false;

    else if( (husky && frenchbulldog && goldenretriever && pitbull && americanbulldog && chihuahua && bostonterrier) ==  false )
      showAllDOGS = true;

    print('American Bulldog: ' + '$americanbulldog');
    print('ShowALlDogs: ' + '$showAllDOGS');
  }

  bool checkforFilterLocations() {
    if( (monterey || santabarbra || sanmarcos || sandiego || sanfrancisco || redding || losangeles ) == true )
      showAllCATS = false;

    else if( (monterey && santabarbra && sanmarcos && sandiego && sanfrancisco && redding && losangeles ) ==  false )
      showAllCATS = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Dog Filters'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.clear_all),
            color: Colors.white,
            tooltip: 'Clear Filters!',
            onPressed: () {
              setState(() {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Text('Cleared Filters!',
                    style: GoogleFonts.zillaSlab(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.lime)),
                  ),
                  duration: Duration(seconds: 2),
                ));
                clearAllDogFilters();
              });
            },
          ),

          IconButton(
            icon: Icon(Icons.keyboard_return),
            color: Colors.white,
            tooltip: 'Save Filters!',
            onPressed: () {
              opendogCategory();
            },
          ),
        ],
        backgroundColor: Colors.green[800],
      ),
      body: ListView(
        children: [
          Row(
            children: [
              Container(
                  height: 50,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(Icons.merge_type),
                      Text('BREEDS', style: TextStyle(fontSize: 20, fontFamily: 'ComicSans', fontWeight: FontWeight.bold, color: Colors.brown)),
                    ],
                  )
              )
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 6,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.brown[600],
                      Colors.brown[400],
                      Colors.brown[200],
                      Colors.brown[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Husky'),
                labelStyle: TextStyle(color: husky ? Colors.black : Colors.black),
                selected: husky,
                onSelected: (bool selected) {
                  setState(() {
                    husky = !husky;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Golden Retriever'),
                labelStyle: TextStyle(color: goldenretriever ? Colors.black : Colors.black),
                selected: goldenretriever,
                onSelected: (bool selected) {
                  setState(() {
                    goldenretriever = !goldenretriever;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Pitbull'),
                labelStyle: TextStyle(color: pitbull ? Colors.black : Colors.black),
                selected: pitbull,
                onSelected: (bool selected) {
                  setState(() {
                    pitbull = !pitbull;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('American Bulldog'),
                labelStyle: TextStyle(color: americanbulldog ? Colors.black : Colors.black),
                selected: americanbulldog,
                onSelected: (bool selected) {
                  setState(() {
                    americanbulldog = !americanbulldog;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Chihuahua'),
                labelStyle: TextStyle(color: chihuahua ? Colors.black : Colors.black),
                selected: chihuahua,
                onSelected: (bool selected) {
                  setState(() {
                    chihuahua = !chihuahua;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),

          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Boston Terrier'),
                labelStyle: TextStyle(color: bostonterrier ? Colors.black : Colors.black),
                selected: bostonterrier,
                onSelected: (bool selected) {
                  setState(() {
                    bostonterrier = !bostonterrier;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('French Bulldog'),
                labelStyle: TextStyle(color: frenchbulldog ? Colors.black : Colors.black),
                selected: frenchbulldog,
                onSelected: (bool selected) {
                  setState(() {
                    frenchbulldog = !frenchbulldog;
                    checkforFilters();
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),

          Row(
            children: [
              Container(
                  height: 50,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(Icons.pin_drop),
                      Text('LOCATIONS', style: TextStyle(fontSize: 20, fontFamily: 'ComicSans', fontWeight: FontWeight.bold, color: Colors.brown)),
                    ],
                  )
              )
            ],
          ),

          Padding(
              padding: EdgeInsets.symmetric(horizontal:10.0),
              child: Container(
                height: 6,
                width: 130.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.brown[600],
                      Colors.brown[400],
                      Colors.brown[200],
                      Colors.brown[100],
                    ],
                    stops: [0.1, 0.3, 0.5, 0.7],
                  ),
                ),
              )
          ),

          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Monterey'),
                labelStyle: TextStyle(color: monterey ? Colors.black : Colors.black),
                selected: monterey,
                onSelected: (bool selected) {
                  setState(() {
                    monterey = !monterey;
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('San Diego'),
                labelStyle: TextStyle(color: sandiego ? Colors.black : Colors.black),
                selected: sandiego,
                onSelected: (bool selected) {
                  setState(() {
                    sandiego = !sandiego;
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Santa Barbra'),
                labelStyle: TextStyle(color: santabarbra ? Colors.black : Colors.black),
                selected: santabarbra,
                onSelected: (bool selected) {
                  setState(() {
                    santabarbra = !santabarbra;
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Los Angeles'),
                labelStyle: TextStyle(color: losangeles ? Colors.black : Colors.black),
                selected: losangeles,
                onSelected: (bool selected) {
                  setState(() {
                    losangeles = !losangeles;
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('San Francisco'),
                labelStyle: TextStyle(color: sanfrancisco ? Colors.black : Colors.black),
                selected: sanfrancisco,
                onSelected: (bool selected) {
                  setState(() {
                    sanfrancisco = !sanfrancisco;
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),

          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('Redding'),
                labelStyle: TextStyle(color: redding ? Colors.black : Colors.black),
                selected: redding,
                onSelected: (bool selected) {
                  setState(() {
                    redding = !redding;
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
              FilterChip(
                padding: EdgeInsets.all(1),
                label: Text('San Marcos'),
                labelStyle: TextStyle(color: sanmarcos ? Colors.black : Colors.black),
                selected: sanmarcos,
                onSelected: (bool selected) {
                  setState(() {
                    sanmarcos = !sanmarcos;
                  });
                },
                selectedColor: Colors.brown[200],
                checkmarkColor: Colors.black,
              ),
            ],
          ),

        ],
      ),
    );
  }
}